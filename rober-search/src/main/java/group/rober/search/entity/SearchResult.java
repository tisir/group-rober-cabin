package group.rober.search.entity;

import org.apache.lucene.document.Document;

import java.util.List;

public class SearchResult {
    protected int hitsCount = 0;
    List<Document> dataList = null;

    public SearchResult() {
    }

    public int getHitsCount() {
        return hitsCount;
    }

    public void setHitsCount(int hitsCount) {
        this.hitsCount = hitsCount;
    }

    public List<Document> getDataList() {
        return dataList;
    }

    public void setDataList(List<Document> dataList) {
        this.dataList = dataList;
    }
}
