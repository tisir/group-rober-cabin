package group.rober.search.exception;

import group.rober.runtime.lang.RoberException;

public class SearchException extends RoberException {
    public SearchException() {
    }

    public SearchException(String message) {
        super(message);
    }

    public SearchException(String messageFormat, Object... objects) {
        super(messageFormat, objects);
    }

    public SearchException(Throwable cause, String messageFormat, Object... objects) {
        super(cause, messageFormat, objects);
    }

    public SearchException(Throwable cause, String message) {
        super(cause, message);
    }

    public SearchException(Throwable cause) {
        super(cause);
    }

    public SearchException(String message, Throwable cause) {
        super(message, cause);
    }

    public SearchException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
