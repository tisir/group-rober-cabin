package group.rober.search.service;

import group.rober.search.entity.SearchResult;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.Sort;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface SearchService {
    void addDocument(Document document) throws IOException;
    void addDocuments(Iterable<? extends Iterable<? extends IndexableField>> documents) throws IOException;

    void deleteDocument(Term... terms) throws IOException;

    public void deleteAllDocument() throws IOException;

    void updateDocument(Term term,Iterable<? extends IndexableField> documents) throws IOException;

    SearchResult searchDocument(String[] fields, String keyWord, Sort sort, int index, int size) throws ParseException, IOException;
}
