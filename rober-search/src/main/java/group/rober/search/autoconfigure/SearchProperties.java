package group.rober.search.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "group.rober.search", ignoreUnknownFields = true)
public class SearchProperties {
    public enum DirectoryMode{Disk,Memory}
    private DirectoryMode directoryMode = DirectoryMode.Memory;
    private String directory = "/data/rober/search" ;

    public DirectoryMode getDirectoryMode() {
        return directoryMode;
    }

    public void setDirectoryMode(DirectoryMode directoryMode) {
        this.directoryMode = directoryMode;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

}
