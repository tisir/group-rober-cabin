package group.rober.search.service;

import group.rober.search.BaseTest;
import group.rober.search.entity.SearchResult;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SearchServiceTest extends BaseTest {

    @Autowired
    protected SearchService searchService;

    @Test
    public void test01() throws IOException, ParseException {
        List<Document> documentList = new ArrayList<Document>();
        documentList.add(createDocument("Lucene in Action", "193398817"));
        documentList.add(createDocument("Lucene for Dummies", "55320055Z"));
        documentList.add(createDocument("Managing Gigabytes", "55063554A"));
        documentList.add(createDocument("The Art of Computer Science", "9900333X"));
        searchService.addDocuments(documentList);

        Sort sort=new Sort(new SortField[]{new SortField("viewCount",SortField.Type.STRING),new SortField("updatedTime",SortField.Type.STRING)});
        SearchResult result = searchService.searchDocument(new String[]{"title"},"lucene",sort,0,100);
        System.out.println("查到结果:"+result.getHitsCount());
        for(Document document : result.getDataList()){
            System.out.println(document.get("isbn") + "\t" + document.get("title"));
        }

        searchService.deleteDocument(new Term("isbn","55320055Z"));
        result = searchService.searchDocument(new String[]{"title"},"lucene",sort,0,100);
        System.out.println("查到结果:"+result.getHitsCount());
        for(Document document : result.getDataList()){
            System.out.println(document.get("isbn") + "\t" + document.get("title"));
        }
    }

    private Document createDocument(String title, String isbn){
        Document doc = new Document();
        doc.add(new TextField("title", title, Field.Store.YES));
        doc.add(new StringField("isbn", isbn, Field.Store.YES));
        return doc;
    }
}
