package group.rober.auth;

import java.util.regex.Pattern;

public interface AuthConsts {
    String SESSION_USER = "sessionUser";
    String REDIRECT_LOCATION_PARAM = "redirectLocation";
    Pattern HTTP_PATTERN = Pattern.compile("^(http|https)",Pattern.CASE_INSENSITIVE);
}
