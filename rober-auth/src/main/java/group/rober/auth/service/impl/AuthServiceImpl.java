package group.rober.auth.service.impl;

import com.google.common.collect.Maps;
import group.rober.auth.entity.Role;
import group.rober.auth.entity.User;
import group.rober.auth.exception.UserNotExistsException;
import group.rober.auth.mapper.RoleMapper;
import group.rober.auth.mapper.UserMapper;
import group.rober.auth.mapper.UserRoleMapper;
import group.rober.auth.service.AuthService;
import group.rober.runtime.kit.StringKit;
import group.rober.sql.core.DataQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Created by tisir<yangsong158@qq.com> on 2017-05-13
 */
@Service("authService")
public class AuthServiceImpl implements AuthService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Resource
    private UserRoleMapper userRoleMapper;
    @Autowired
    private DataQuery dataQuery;


    public List<User> selectUserList(String where, Map<String, Object> vars) {
        StringBuilder sql = new StringBuilder("select * from AUTH_USER");
        if(StringKit.isNotBlank(where)){
            sql.append(" where ").append(where);
        }
        sql.append(" order by ID asc");
        return dataQuery.selectList(User.class,sql.toString(),vars);
    }

    public User getUser(String userId) {
        User user = userMapper.selectById(userId);
        return user;
    }

    public User getUserByCode(String userCode) {
        return userMapper.selectByCode(userCode);
    }

    public int saveUser(User user) {
        boolean insertModel = true;
        //有id,则更新
        if(user.getId() != null){
            User exists = getUser(user.getId());
            if(exists != null){//存在,则更新
                insertModel = false;
            }
        }

        if(insertModel){
            userMapper.insert(user);
        }else{
            userMapper.update(user);
        }
        return 1;
    }

    public int deleteUser(String userId) {
        userMapper.deleteById(userId);
        return 1;
    }

    public Map<String, String> getUserProperties(String userId) {
        return null;
    }

    public int addUserProperty(String userId, String propertyName, String propertyValue) {
        return 0;
    }

    public int removeUserProperty(String userId, String propertyName) {
        return 0;
    }

    public boolean verifyPassword(String userId, String password) {
        User user = userMapper.selectById(userId);
        if(user!=null){
            return password.equals(user.getPassword());
        }else{
            throw new UserNotExistsException("user.id={0}",userId);
        }

    }

    public void updatePassword(String userId, String password) {
        userMapper.updatePassword(userId,password);
//        User user = userMapper.selectById(userId);
//        if(user!=null){
//            user.setPassword(password);
//            userMapper.update(user);
//        }else{
//            throw new UserNotExistsException("user.id={0}",userId);
//        }
    }

    public Role getRole(String roleId) {
        return roleMapper.selectById(roleId);
    }

    public Role getRoleByCode(String roleCode) {
        return roleMapper.selectByCode(roleCode);
    }

    public int saveRole(Role role) {
        boolean insertModel = true;
        //有id,则更新
        if(role.getId() != null){
            Role exists = roleMapper.selectById(role.getId());
            if(exists != null){//存在,则更新
                insertModel = false;
            }
        }

        if(insertModel){
            roleMapper.insert(role);
        }else{
            roleMapper.update(role);
        }
        return 1;
    }

    public int deleteRole(String roleId) {
        roleMapper.deleteById(roleId);
        return 1;
    }

    public List<Role> getUserRoles(String userId) {
        return roleMapper.selectRolesByUser(userId);
    }

    public int grantRoleToUser(String roleId, String userId) {
        userRoleMapper.delete(userId,roleId);   //先删除可能存在的关联

        Map<String,Object> userRole = Maps.newHashMap();
        userRole.put("id",userRoleMapper.selectMaxId()+1);
        userRole.put("userId",userId);
        userRole.put("roleId",roleId);
        userRoleMapper.insert(userRole);

        return 1;
    }

    public int revokeRoleFormUser(String roleId, String userId) {
        userRoleMapper.delete(userId,roleId);
        return 1;
    }

    public boolean userHasRole(String userId, String roleId) {
        return userRoleMapper.selectCountByUserIdAndRoleId(userId,roleId)>0;
    }

    public boolean userHasRoleByCode(String userCode, String roleCode) {
        return userRoleMapper.selectCountByUserCodeAndRoleCode(userCode,roleCode)>0;
    }

}
