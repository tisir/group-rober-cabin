package group.rober.auth.service;


import group.rober.auth.entity.Role;
import group.rober.auth.entity.User;

import java.util.List;
import java.util.Map;

/**
 * Created by tisir<yangsong158@qq.com> on 2017-05-13
 */
public interface AuthService {

    public List<User> selectUserList(String where, Map<String, Object> vars);
    public User getUser(String id);
    public User getUserByCode(String userCode);
    public int saveUser(User user);
    public int deleteUser(String userId);
    public Map<String,String> getUserProperties(String userId);
    public int addUserProperty(String userId, String propertyName, String propertyValue);
    public int removeUserProperty(String userId, String propertyName);

    public boolean verifyPassword(String userId, String password);
    void updatePassword(String userId, String password);


    public Role getRole(String roleId);
    public Role getRoleByCode(String roleCode);
    public int saveRole(Role role);
    public int deleteRole(String roleId);
    public List<Role> getUserRoles(String userId);
    public int grantRoleToUser(String roleId, String userId);
    public int revokeRoleFormUser(String roleId, String userId);
    public boolean userHasRoleByCode(String userCode, String roleCode);
    public boolean userHasRole(String userId, String roleId);
}
