package group.rober.auth.shiro;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import group.rober.auth.AuthConsts;
import group.rober.auth.entity.Role;
import group.rober.auth.entity.User;
import group.rober.auth.service.AuthService;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;

/**
 * 继承 AuthorizingRealm 实现认证和授权2个方法
 * Created by tisir<yangsong158@qq.com> on 2017-05-14
 */
public class AuthRealm extends AuthorizingRealm {

    private static final Logger logger = LoggerFactory.getLogger(AuthRealm.class);

    @Autowired
    private AuthService authService;

    public AuthService getAuthService() {
        return authService;
    }

    public void setAuthService(AuthService authService) {
        this.authService = authService;
    }

    /**
     * 权限认证，为当前登录的Subject授予角色和权限
     * 经测试：本例中该方法的调用时机为需授权资源被访问时
     * 经测试：并且每次访问需授权资源时都会执行该方法中的逻辑，这表明本例中默认并未启用AuthorizationCache
     * 经测试：如果连续访问同一个URL（比如刷新），该方法不会被重复调用，Shiro有一个时间间隔（也就是cache时间，在ehcache-shiro.xml中配置），超过这个时间间隔再刷新页面，该方法会被执行
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        logger.info("##################执行Shiro权限认证##################");
        //获取当前登录输入的用户名，等价于(String) principalCollection.fromRealm(getName()).iterator().next();
        String userCode = (String)super.getAvailablePrincipal(principalCollection);
        //到数据库查是否有此对象
        User user = authService.getUser(userCode);// 实际项目中，这里可以根据实际情况做缓存，如果不做，Shiro自己也是有时间间隔机制，2分钟内不会重复执行该方法
        if(user!=null){
            //权限信息对象info,用来存放查出的用户的所有的角色（role）及权限（permission）
            SimpleAuthorizationInfo info=new SimpleAuthorizationInfo();
            //用户的角色集合
            List<Role> roles = authService.getUserRoles(user.getId());
            Function<Role,String> transFunc = new Function<Role,String>(){
                public String apply(Role role) {
                    return role.getName();
                }
            };
            Set<String> roleNames = ImmutableSet.<String>builder().addAll(Lists.transform(roles,transFunc)).build();
            info.setRoles(roleNames);

//            //用户的角色对应的所有权限，如果只使用角色定义访问权限，下面的四行可以不要
//            for (Role role : roles) {
//                info.addStringPermissions(role.getPermissionsName());
//            }
            // 或者按下面这样添加
            //添加一个角色,不是配置意义上的添加,而是证明该用户拥有admin角色
//            simpleAuthorInfo.addRole("admin");
            //添加权限
//            simpleAuthorInfo.addStringPermission("admin:manage");
//            logger.info("已为用户[mike]赋予了[admin]角色和[admin:manage]权限");
            return info;
        }
        // 返回null的话，就会导致任何用户访问被拦截的请求时，都会自动跳转到unauthorizedUrl指定的地址
        return null;
    }

    /**
     * 登录认证
     */
    protected AuthenticationInfo doGetAuthenticationInfo(
            AuthenticationToken authenticationToken) throws AuthenticationException {
        //UsernamePasswordToken对象用来存放提交的登录信息
        UsernamePasswordToken token=(UsernamePasswordToken) authenticationToken;

        logger.debug("验证当前Subject时获取到token为：" + ReflectionToStringBuilder.toString(token, ToStringStyle.MULTI_LINE_STYLE));

        //查出是否有此用户
        User user = authService.getUserByCode(token.getUsername());   //userName是shiro中的概念,等同于我们的userCode
        if(user == null) {
            throw new UnknownAccountException(token.getUsername());//没找到帐号
        }
        if("locked".equals(user.getStatus())) {
            throw new LockedAccountException(); //帐号锁定
        }
//        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
//                user.getCode(), //用户名
//                user.getPassword(), //密码
//                ByteSource.Util.bytes(user.getCredentialsSalt()),//salt=username+salt
//                getName()  //realm name
//        );
        //若存在，将此用户存放到登录认证info中，无需自己做密码对比，Shiro会为我们进行密码对比校验
        //交给AuthenticatingRealm使用CredentialsMatcher进行密码匹配，如果觉得人家的不好可以在此判断或自定义实现
        //把用户放到会话对象中去
        SecurityUtils.getSubject().getSession().setAttribute(AuthConsts.SESSION_USER,user);
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                user.getCode(), //用户名
                user.getPassword(), //密码,数据库中的密码
                getName()  //realm name
        );

        return authenticationInfo;
    }
}
