package group.rober.auth.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import group.rober.auth.entity.Org;

/**
 * Created by tisir<yangsong158@qq.com> on 2017-05-12
 */
public interface OrgMapper extends BaseMapper<Org> {
}
