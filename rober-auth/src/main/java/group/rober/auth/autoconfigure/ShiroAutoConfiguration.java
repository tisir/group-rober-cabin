package group.rober.auth.autoconfigure;

import group.rober.auth.service.AuthService;
import group.rober.auth.shiro.AuthRealm;
import group.rober.auth.shiro.ClearTextCredentialsMatcher;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.session.mgt.SessionValidationScheduler;
import org.apache.shiro.session.mgt.eis.EnterpriseCacheSessionDAO;
import org.apache.shiro.session.mgt.eis.JavaUuidSessionIdGenerator;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.session.mgt.eis.SessionIdGenerator;
import org.apache.shiro.session.mgt.quartz.QuartzSessionValidationScheduler;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.Cookie;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * shiro的@Bean注解有问题，这里使用xml文件配置<br>
 *     另一方面，又要考虑统一使用application.yaml中的配置项，因此，这里的自动注解会继续使用，在配置文件中使用SPEL表达式引到配置文件中
 */
@Configuration("shiroAutoConfiguration")
@EnableConfigurationProperties(ShiroProperties.class)
public class ShiroAutoConfiguration {
    protected ShiroProperties properties;

    public ShiroAutoConfiguration(ShiroProperties properties) {
        this.properties = properties;
    }

    public ShiroProperties getProperties() {
        return properties;
    }

    public void setProperties(ShiroProperties properties) {
        this.properties = properties;
    }

    @Bean("credentialsMatcher")
    public CredentialsMatcher getSimpleCredentialsMatcher(){
        return new SimpleCredentialsMatcher();
    }
}
