package group.rober.auth.autoconfigure;


import com.jagregory.shiro.freemarker.ShiroTags;
import group.rober.auth.interceptor.AuthHolderInterceptor;
import group.rober.auth.tags.AuthTags;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@ImportResource("classpath:group/rober/auth/application-context.xml")
@EnableConfigurationProperties(AuthProperties.class)
@ComponentScan(basePackages = "group.rober.auth")   //要加上这句，否则此模块被其他模块引用时，自动@Component注解的类无法使用
@MapperScan(basePackages = "group.rober.auth.mapper")   //加上这句，指定mybatis包的扫描路径
public class AuthAutoConfiguration  extends WebMvcConfigurerAdapter implements InitializingBean {

    protected AuthProperties properties;

    @Autowired
    private freemarker.template.Configuration configuration;
    @Autowired
    private ShiroProperties shiroProperties;

    public AuthAutoConfiguration(AuthProperties properties) {
        this.properties = properties;
    }

    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AuthHolderInterceptor()).addPathPatterns("/**");
    }

    public void afterPropertiesSet() throws Exception {
        // 加上这句后，可以在页面上使用shiro标签
        configuration.setSharedVariable(shiroProperties.getTagName(), new ShiroTags());
        configuration.setSharedVariable(properties.getTagName(), new AuthTags());
        configuration.setNumberFormat("#");
    }
}
