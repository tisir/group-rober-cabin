package group.rober.auth.autoconfigure;


import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "group.rober.auth", ignoreUnknownFields = true)
public class AuthProperties {
    private String tagName = "auth";

    private String createdByPropertyName = "createdBy";

    private String updatedByPropertyName = "updatedBy";

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getCreatedByPropertyName() {
        return createdByPropertyName;
    }

    public void setCreatedByPropertyName(String createdByPropertyName) {
        this.createdByPropertyName = createdByPropertyName;
    }

    public String getUpdatedByPropertyName() {
        return updatedByPropertyName;
    }

    public void setUpdatedByPropertyName(String updatedByPropertyName) {
        this.updatedByPropertyName = updatedByPropertyName;
    }
}
