drop table if exists DEMO_PERSON;

/*==============================================================*/
/* Table: DEMO_PERSON                                           */
/*==============================================================*/
create table DEMO_PERSON
(
   ID                   int not null comment '个人编号',
   CODE                 varchar(32) not null comment '用户代号',
   NAME                 varchar(150) comment '名称',
   CHN_NAME             varchar(150) comment '中文名',
   ENG_NAME             varchar(150) comment '英文名',
   AVATAR               varchar(300) comment '头像',
   GENDER               varchar(32) comment '性别',
   BIRTH                datetime comment '出生日期',
   HEIGHT               numeric(24,6) comment '身高',
   WEIGHT               numeric(24,6) comment '体重',
   NATION               varchar(32) comment '名族',
   POLITICAL            varchar(32) comment '政治面貌',
   MARITAL              varchar(32) comment '婚姻状况',
   EDUCATION_LEVEL      varchar(32) comment '最高学历',
   GRADUATED_FROM       varchar(300) comment '毕业院校',
   DOMICILE_PLACE_PROVINCE varchar(32) comment '籍贯（省）',
   DOMICILE_PLACE_CITY  varchar(32) comment '籍贯（市）',
   DOMICILE_PLACE_ADDRESS varchar(300) comment '户籍地址',
   PRESENT_ADDRESS      varchar(300) comment '居住地址',
   MOBILE_PHONE         varchar(120) comment '手机号',
   EMAIL                varchar(120) comment '电子邮件',
   WORK_AS              varchar(300) comment '职业',
   JOB_TITLE            varchar(32) comment '职称',
   COMPANY_INDUSTRY     varchar(32) comment '单位所属行业',
   COMPANY_ADDRESS      varchar(300) comment '单位地址',
   COMPANY_POSTCODE     varchar(30) comment '单位邮编',
   ENTRY_DATE           datetime comment '入职日期',
   MONTH_INCOME         numeric(24,6) comment '个人月收入',
   FAMILY_MONTH_INCOME  numeric(24,6) comment '家庭月收入',
   FAMILY_YEAR_INCOME   numeric(24,6) comment '家庭年收入',
   FAMILY_MONTH_COST    numeric(24,6) comment '家庭每月固定支出',
   HOBBY                varchar(900) comment '爱好',
   REMARK               varchar(900) comment '备注',
   STATUS               varchar(32) comment '状态',
   REVISION             int comment '乐观锁版本',
   CREATED_BY           int comment '创建人',
   CREATED_TIME         datetime comment '创建时间',
   UPDATED_BY           int comment '更新人',
   UPDATED_TIME         datetime comment '更新时间',
   primary key (ID)
);

alter table DEMO_PERSON comment '个人信息';
