package group.rober.runtime.web;

import group.rober.runtime.lang.MapData;
import group.rober.runtime.lang.ValueObject;

import javax.servlet.http.HttpServletRequest;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 请求参数再次封装
 * Created by tisir<yangsong158@qq.com> on 2017-05-31
 */
public class RequestParameterMap extends MapData {


	private static final long serialVersionUID = -3167615295990846332L;

	public RequestParameterMap(){

    }
	public RequestParameterMap(HttpServletRequest request){
        Map<String, String[]> parameterMap = request.getParameterMap();
        this.merge(parameterMap);
    }

    public RequestParameterMap merge(Map<String,?> map){
        Iterator<String> iterator = map.keySet().iterator();
        while (iterator.hasNext()){
            String name = iterator.next();
            Object value = map.get(name);
            Object[] valueArray = null;
            if(value!=null&&value.getClass().isArray()){
                valueArray = (Object[])value;
                if(valueArray.length>1){
                    this.put(name,new ValueObject(valueArray));
                }else if(valueArray.length==1){
                    this.put(name,new ValueObject(valueArray[0]));
                }else{
                    this.put(name,new ValueObject(null));
                }
            }else{
                this.put(name,new ValueObject(value));
            }
        }
        return this;
    }

}
