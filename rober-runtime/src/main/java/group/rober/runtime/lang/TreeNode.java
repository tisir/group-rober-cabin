package group.rober.runtime.lang;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 定义树形节点
 * @author 杨松<syang@amarsoft.com>
 *
 */
public class TreeNode<T> implements Serializable,Cloneable{

	private static final long serialVersionUID = -1863294836839490555L;

	private TreeNode<T> parent = null;

	private T value = null;

	private List<TreeNode<T>> children = new ArrayList<TreeNode<T>>();

	public TreeNode(){

	}
	public TreeNode(TreeNode<T> parent) {
		this.parent = parent;
	}
	public TreeNode(TreeNode<T> parent, T value) {
		this.parent = parent;
		this.value = value;
	}
	public TreeNode(T value) {
		this.value = value;
	}

	public TreeNode<T> getParent() {
		return parent;
	}
	public void setParent(TreeNode<T> parent) {
		this.parent = parent;
	}
	public T getValue() {
		return value;
	}
	public void setValue(T value) {
		this.value = value;
	}
	public List<TreeNode<T>> getChildren() {
		return children;
	}
	public void setChildren(List<TreeNode<T>> children) {
		this.children = children;
	}
	public void addChild(TreeNode<T> value){
		this.children.add(value);
	}
	public void removeChild(T value){
		this.children.remove(value);
	}
	/**
	 * 查找一个子树
	 * @param v
	 * @return
	 */
	public TreeNode<T> downFind(T v){
		for(int i=0;i<children.size();i++){
			TreeNode<T> node = children.get(i);
			if(node.getValue() == v){
				return node;
			}else if(value.equals(v)){
				return node;
			}
		}
		for(int i=0;i<children.size();i++){
			TreeNode<T> node = children.get(i).downFind(v);
			if(node!=null)return node;
		}
		return null;
	}



}
