package group.rober.runtime.model;

import group.rober.runtime.kit.ListKit;
import group.rober.runtime.lang.MapData;

import java.io.Serializable;
import java.util.List;

/**
 * 节点条目,一般用来组织具有上下级关系的可伸缩的节点
 * Created by tisir<yangsong158@qq.com> on 2017-06-06
 */
public class NodeEntry extends MapData implements Serializable,Cloneable{

	private static final long serialVersionUID = 5443667887753869721L;

	public static final String KEY_PARENT = "parent";
    public static final String KEY_CHILDREN = "children";

    public NodeEntry getParent() {
        return this.getObject(KEY_PARENT,NodeEntry.class);
    }

    public void setParent(NodeEntry parent) {
        this.putValue(KEY_PARENT,parent);
    }

    @SuppressWarnings("unchecked")
	public List<NodeEntry> getChildren() {
        return (List<NodeEntry>)this.getObject(KEY_CHILDREN,List.class);
    }

    public void setChildren(List<NodeEntry> children) {
        this.putValue(KEY_CHILDREN,children);
    }

    public NodeEntry addChild(NodeEntry nodeEntry){
        List<NodeEntry> children = getChildren();
        if(children==null){
            children = ListKit.newArrayList();
            setChildren(children);
        }
        children.add(nodeEntry);

        return this;
    }
}
