package group.rober.runtime.kit;

import group.rober.runtime.lang.RoberException;
import org.apache.commons.io.IOUtils;

import java.io.Closeable;
import java.io.IOException;

/**
 * IO常用工具类
 * Created by tisir<yangsong158@qq.com> on 2017-02-19
 */
public abstract class IOKit extends IOUtils {
    public static void close(Closeable stream){
        try {
            if (stream != null) stream.close();
        } catch (IOException e) {
            new RoberException("close {0} error",stream.getClass().getName());
        }
    }

}
