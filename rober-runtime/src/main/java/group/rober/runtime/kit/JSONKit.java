package group.rober.runtime.kit;

//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONObject;
//import com.alibaba.fastjson.serializer.SerializeConfig;
//import com.alibaba.fastjson.serializer.SerializerFeature;
//import com.alibaba.fastjson.serializer.SimpleDateFormatSerializer;
//import org.teamsir.basic.holders.ContextHolder;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.util.List;

/**
 * Created by tisir<yangsong158@qq.com> on 2017-02-18
 */
public abstract class JSONKit {

    static SerializeConfig serializeConfig = new SerializeConfig();

    public static String toJsonString(Object object){
        if(object == null) return null;

//        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        //日期的格式化处理(然而设置了,也没什么卵用,进去看了下fastjson的源代码,你设置了,人家根本不用,这个方法我暂时不采用)
//        serializeConfig.put(Date.class, new SimpleDateFormatSerializer(DateKit.DATE_TIME_FORMAT));
//        String str = JSON.toJSONStringWithDateFormat(object,DateKit.DATE_TIME_FORMAT,
//                SerializerFeature.WriteMapNullValue,
//                SerializerFeature.PrettyFormat,
//                SerializerFeature.WriteNonStringKeyAsString);

        String str = JSON.toJSONString(object,
                serializeConfig,
                null,
                DateKit.DATE_TIME_FORMAT,
                JSON.DEFAULT_GENERATE_FEATURE,
                SerializerFeature.WriteMapNullValue,
                SerializerFeature.PrettyFormat,
                SerializerFeature.WriteNonStringKeyAsString);
        return str;
    }

    public static String toJsonStringNowrap(Object object){
        if(object == null) return null;

//        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        //日期的格式化处理(然而设置了,也没什么卵用,进去看了下fastjson的源代码,你设置了,人家根本不用,这个方法我暂时不采用)
//        serializeConfig.put(Date.class, new SimpleDateFormatSerializer(DateKit.DATE_TIME_FORMAT));
//        String str = JSON.toJSONStringWithDateFormat(object,DateKit.DATE_TIME_FORMAT,
//                SerializerFeature.WriteMapNullValue,
//                SerializerFeature.PrettyFormat,
//                SerializerFeature.WriteNonStringKeyAsString);

        String str = JSON.toJSONString(object,
                serializeConfig,
                null,
                DateKit.DATE_TIME_FORMAT,
                JSON.DEFAULT_GENERATE_FEATURE,
                SerializerFeature.WriteMapNullValue,
                SerializerFeature.WriteNonStringKeyAsString);
        return str;
    }

    public static <T>T jsonToBean(String text,Class<T> classType){
        return JSONObject.parseObject(text, classType);
    }

    public static <T> List<T> jsonToBeanList(String text, Class<T> classType){
        return JSONArray.parseArray(text,classType);
    }
}
