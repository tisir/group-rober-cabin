package group.rober.runtime.kit;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by cytsir on 17/2/16.
 */
public abstract class DateKit {
    public static final String DATE_TIME_FORMAT="yyyy-MM-dd HH:mm:ss.SSS";

    public static Date now(){
        return new Date();
    }
    public static Date parse(String str){
        return DateTime.parse(str).toDate();
    }
    public static Date parse(Long value){
        return new Date(value);
    }
    public static int getYear(Date date){
        DateTime dateTime = new DateTime(date);
        return dateTime.getYear();
    }

    public static int getYearOfCentury(Date date){
        DateTime dateTime = new DateTime(date);
        return dateTime.getYearOfCentury();
    }

    public static String format(Date date){
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_TIME_FORMAT);
        return dateFormat.format(date);
    }

    /**
     * 取两个日期间隔的天数
     * @param date1
     * @param date2
     * @return
     */
    public static int getDaysApart(Date date1,Date date2){
        DateTime begin = new DateTime(date1);
        DateTime end = new DateTime(date2);
        Period period = new Period(begin,end, PeriodType.days());
        return period.getDays();
    }
    /**
     * 取两个日期间隔的天数
     * @param date1
     * @param date2
     * @return
     */
    public static int getMonthsApart(Date date1,Date date2){
        DateTime begin = new DateTime(date1);
        DateTime end = new DateTime(date2);
        Period period = new Period(begin,end, PeriodType.months());
        return period.getMonths();
    }

}
