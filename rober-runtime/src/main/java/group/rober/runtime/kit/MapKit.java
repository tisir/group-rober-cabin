package group.rober.runtime.kit;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import java.util.HashMap;
import java.util.Map;

/**
 * 把常用的对Map的操作方法放这里来<br/>
 * 主要是放guava中对Map的操作,把常用的放这里,是为了简化操作,也方便升级调整
 * Created by tisir<yangsong158@qq.com> on 2017-05-20
 */
public abstract class MapKit {

    public static Map<String,Object> newEmptyMap(){
        return new HashMap<String,Object>();
    }

    public static <K, V> Map<K, V> newHashMap() {
        return Maps.newHashMap();
    }

    public static <K, V> Map<K, V> newLinkedHashMap() {
        return Maps.newLinkedHashMap();
    }

    public static <K, V> Map<K, V> mapOf(K k1, V v1) {
        return ImmutableMap.of(k1, v1);
    }

    public static <K, V> Map<K, V> mapOf(K k1, V v1, K k2, V v2) {
        return ImmutableMap.of(k1, v1, k2, v2);
    }

    public static <K, V> Map<K, V> mapOf(K k1, V v1, K k2, V v2, K k3, V v3) {
        return ImmutableMap.of(k1, v1, k2, v2, k3, v3);
    }

    public static <K, V> Map<K, V> mapOf(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4) {
        return ImmutableMap.of(k1, v1, k2, v2, k3, v3,k4,v4);
    }
    public static <K, V> Map<K, V> mapOf(K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return ImmutableMap.of(k1, v1, k2, v2, k3, v3,k4,v4,k5,v5);
    }
}
