package group.rober.runtime.kit;

import com.google.common.base.CaseFormat;
import com.google.common.base.Joiner;
import group.rober.runtime.lang.BasicConstant;
import org.apache.commons.lang3.StringUtils;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Map;

/**
 * Created by tisir<yangsong158@qq.com> on 2017-04-06
 */
public abstract class StringKit extends StringUtils{
    /**
     * 空串
     * @param s 字串
     * @param s1 字串为空时，返回的字串
     * @return
     */
    public static String nvl(Object s,String s1){
        String ss = "";
        if(s instanceof String){
            ss = (String)s;
        }else{
            ss = String.valueOf(s);
        }

        if(StringUtils.isEmpty(ss))return s1;
        return ss;
    }

    /**
     * 把驼峰字串变为下划线变量，全部大写。例如：PersonName将被转为 PERSON_NAME
     * @param str
     * @return
     */
    public static String upperUnderscore(String str){
        return CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, str);
    }

    /**
     * 把下划线风格转驼峰,例如:PERSON_NAME转为PersonName
     * @param str
     * @return
     */
    public static String lowerCamel(String str){
        return CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, str);
    }

    /**
     * 连接字串
     * @param parts
     * @param delimiter
     * @return
     */
    public static String join(Iterable<?> parts, String delimiter){
        return Joiner.on(delimiter).join(parts);
    }

    /**
     * 连接MAP类型的字串
     * @param map
     * @param keyValueSeparator
     * @param delimiter
     * @return
     */
    public static String join(Map<?, ?> map, String keyValueSeparator, String delimiter){
        Joiner.MapJoiner joiner = Joiner.on(delimiter).withKeyValueSeparator(keyValueSeparator);
        return joiner.join(map);
    }
//    /**
//     * 连接字串
//     * @param strList
//     * @param delimiter
//     * @return
//     */
//    @SuppressWarnings("unchecked")
//    public static String join(String[] strList, String delimiter){
//        return join(Arrays.asList(strList));
//    }

    public static String trim(String str) {
        return str == null ? null : str.trim();
    }

    public static boolean equals(String str1, String str2) {
        return str1 == null ? str2 == null : str1.equals(str2);
    }
    public static String[] toStringArray(Collection<String> collection) {
        if (collection == null) {
            return null;
        }
        return collection.toArray(new String[collection.size()]);
    }

    // ==========================================================================
    // Perl风格的chomp和chop函数。
    // ==========================================================================

    /**
     * 删除字符串末尾的换行符。如果字符串不以换行结尾，则什么也不做。
     * <p>
     * 换行符有三种情形：&quot;<code>\n</code>&quot;、&quot;<code>\r</code>&quot;、&quot;
     * <code>\r\n</code>&quot;。
     * <p/>
     * <pre>
     * chomp(null)          = null
     * chomp("")            = ""
     * chomp("abc \r")      = "abc "
     * chomp("abc\n")       = "abc"
     * chomp("abc\r\n")     = "abc"
     * chomp("abc\r\n\r\n") = "abc\r\n"
     * chomp("abc\n\r")     = "abc\n"
     * chomp("abc\n\rabc")  = "abc\n\rabc"
     * chomp("\r")          = ""
     * chomp("\n")          = ""
     * chomp("\r\n")        = ""
     * </pre>
     * <p/>
     * </p>
     *
     * @param str 要处理的字符串
     * @return 不以换行结尾的字符串，如果原始字串为<code>null</code>，则返回<code>null</code>
     */
    public static String chomp(String str) {
        if (str == null || str.length() == 0) {
            return str;
        }

        if (str.length() == 1) {
            char ch = str.charAt(0);

            if (ch == '\r' || ch == '\n') {
                return BasicConstant.EMPTY_STRING;
            } else {
                return str;
            }
        }

        int lastIdx = str.length() - 1;
        char last = str.charAt(lastIdx);

        if (last == '\n') {
            if (str.charAt(lastIdx - 1) == '\r') {
                lastIdx--;
            }
        } else if (last == '\r') {
        } else {
            lastIdx++;
        }

        return str.substring(0, lastIdx);
    }

//    /**
//     * 删除字符串末尾的指定字符串。如果字符串不以该字符串结尾，则什么也不做。
//     * <p/>
//     * <pre>
//     * chomp(null, *)         = null
//     * chomp("", *)           = ""
//     * chomp("foobar", "bar") = "foo"
//     * chomp("foobar", "baz") = "foobar"
//     * chomp("foo", "foo")    = ""
//     * chomp("foo ", "foo")   = "foo "
//     * chomp(" foo", "foo")   = " "
//     * chomp("foo", "foooo")  = "foo"
//     * chomp("foo", "")       = "foo"
//     * chomp("foo", null)     = "foo"
//     * </pre>
//     *
//     * @param str       要处理的字符串
//     * @param separator 要删除的字符串
//     * @return 不以指定字符串结尾的字符串，如果原始字串为<code>null</code>，则返回<code>null</code>
//     */
//    public static String chomp(String str, String separator) {
//        if (str == null || str.length() == 0 || separator == null) {
//            return str;
//        }
//
//        if (str.endsWith(separator)) {
//            return str.substring(0, str.length() - separator.length());
//        }
//
//        return str;
//    }

    /**
     * 删除最后一个字符。
     * <p>
     * 如果字符串以<code>\r\n</code>结尾，则同时删除它们。
     * <p/>
     * <pre>
     * chop(null)          = null
     * chop("")            = ""
     * chop("abc \r")      = "abc "
     * chop("abc\n")       = "abc"
     * chop("abc\r\n")     = "abc"
     * chop("abc")         = "ab"
     * chop("abc\nabc")    = "abc\nab"
     * chop("a")           = ""
     * chop("\r")          = ""
     * chop("\n")          = ""
     * chop("\r\n")        = ""
     * </pre>
     * <p/>
     * </p>
     *
     * @param str 要处理的字符串
     * @return 删除最后一个字符的字符串，如果原始字符串为<code>null</code>，则返回<code>null</code>
     */
    public static String chop(String str) {
        if (str == null) {
            return null;
        }

        int strLen = str.length();

        if (strLen < 2) {
            return BasicConstant.EMPTY_STRING;
        }

        int lastIdx = strLen - 1;
        String ret = str.substring(0, lastIdx);
        char last = str.charAt(lastIdx);

        if (last == '\n') {
            if (ret.charAt(lastIdx - 1) == '\r') {
                return ret.substring(0, lastIdx - 1);
            }
        }

        return ret;
    }

    public static String format(String tpl,Object... args){
        return MessageFormat.format(tpl,args);
    }


}
