package group.rober.runtime.kit;

//import group.rober.runtime.jdbc.NameConverter;
import group.rober.runtime.lang.MapData;
import org.apache.commons.io.IOUtils;
import org.springframework.jdbc.support.JdbcUtils;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Date;
import java.util.Map;

/**
 * Created by tisir<yangsong158@qq.com> on 2017-02-19
 */
public abstract class SQLKit {

    /**
     * SQL的日期转JAVA日期
     * @param sqlDate
     * @return
     */
    public static Date javaDate(java.sql.Date sqlDate){
        if(sqlDate==null)return null;
        Date date = new Date (sqlDate.getTime());
        return date;
    }
    public static Date javaDate(java.sql.Time time){
        if(time==null)return null;
        Date date = new Date (time.getTime());
        return date;
    }
    public static Date javaDate(java.sql.Timestamp time){
        if(time==null)return null;
        Date date = new Date (time.getTime());
        return date;
    }

    /**
     * JAVA日期转SQL日期
     * @param javaDate
     * @return
     */
    public static java.sql.Date sqlDate(Date javaDate){
        if(javaDate==null)return null;
        return new java.sql.Date(javaDate.getTime());
    }

//    public static MapData getRowValue(ResultSet rs, Map<Integer,String> columnPropertyMap) throws SQLException{
//        MapData rowObject = new MapData();
//
//        ResultSetMetaData metaData = rs.getMetaData();
//        int columnCount = metaData.getColumnCount();
//        for(int i=1;i<=columnCount;i++){
//            Object value = JdbcUtils.getResultSetValue(rs,i);
//            String propertyName = columnPropertyMap.get(i);
//            if(StringKit.isBlank(propertyName))continue;
//            rowObject.put(propertyName,value);
//        }
//
//        return rowObject;
//    }

//    public static DataObject getRowValue(ResultSet rs, NameConverter converter) throws SQLException {
//        DataObject row = new DataObject();
//
//        ResultSetMetaData meta = rs.getMetaData();
//        int cols = meta.getColumnCount();
//        for(int i=1;i<=cols;i++){
//            String columnName = meta.getColumnName(i);
//            Object value = JdbcUtils.getResultSetValue(rs,i);;
//            //从两个方向取
//            String propName = converter.getPropertyName(columnName);
//            if(StringKit.isBlank(propName))propName = converter.getPropertyName(i);
//
//            row.put(propName,value);
//        }
//
//        return row;
//    }

    /**
     * 取结果集中,查询出来的原始数据
     * @param rs 结果集
     * @param i 指定行数据的列索引
     * @return
     * @throws SQLException
     * @throws IOException
     */
    public static Object getCellValue(ResultSet rs, int i) throws SQLException, IOException {
        ResultSetMetaData metaData = rs.getMetaData();
        int type = metaData.getColumnType(i);
        switch (type){
            case Types.VARCHAR:
            case Types.CHAR:
            case Types.NCHAR:
            case Types.LONGNVARCHAR:
            case Types.LONGVARBINARY:
            case Types.NVARCHAR:
                return rs.getString(i);
            case Types.CLOB:
            case Types.NCLOB:
                return IOUtils.toString(rs.getClob(i).getCharacterStream());
            case Types.BOOLEAN:
                return rs.getBoolean(i);
            case Types.DATE:
                return SQLKit.javaDate(rs.getDate(i));
            case Types.TIME:
                return SQLKit.javaDate(rs.getTime(i));
            case Types.TIMESTAMP:
                return SQLKit.javaDate(rs.getTimestamp(i));
            case Types.INTEGER:
            case Types.TINYINT:
            case Types.SMALLINT:
                return rs.getInt(i);
            case Types.BIGINT:
                return rs.getLong(i);
            case Types.DOUBLE:
            case Types.DECIMAL:
            case Types.NUMERIC:
            case Types.FLOAT:
                return rs.getDouble(i);
            case Types.NULL:
                return null;
            default:
                rs.getString(i);
        }
        return null;
    }

}
