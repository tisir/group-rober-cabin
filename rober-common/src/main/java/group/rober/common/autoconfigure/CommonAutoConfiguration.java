package group.rober.common.autoconfigure;

import group.rober.common.CommonConsts;
import group.rober.common.service.FileManageService;
import group.rober.runtime.kit.ApplicationContextKit;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(CommonProperties.class)
public class CommonAutoConfiguration implements BeanDefinitionRegistryPostProcessor {


    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        //注册图片文件管理服务
        ApplicationContextKit.registerBean(registry,CommonConsts.IMG_FILE_SERVICE_NAME,FileManageService.class);
        //注册图片文档管理服务
        ApplicationContextKit.registerBean(registry,CommonConsts.DOC_FILE_SERVICE_NAME,FileManageService.class);
//        System.out.println("--*****************----2---*****************--");
    }

//    @Bean(CommonConsts.DOC_FILE_SERVICE_NAME)
////    @ConditionalOnClass(FileManageService.class)
////    @ConditionalOnBean(name=CommonConsts.DOC_FILE_SERVICE_NAME)
//    public FileManageService getFileManageService(){
//        System.out.println("--*****************----1---*****************--");
//        return new FileManageService();
//    }

    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

    }
}
