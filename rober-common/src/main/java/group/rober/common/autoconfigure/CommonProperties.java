package group.rober.common.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "group.rober.common", ignoreUnknownFields = true)
public class CommonProperties {
    private String fileStorageRoot = "/data/rober";
    private int fileBufferSize = 4096;

    public String getFileStorageRoot() {
        return fileStorageRoot;
    }

    public void setFileStorageRoot(String fileStorageRoot) {
        this.fileStorageRoot = fileStorageRoot;
    }

    public int getFileBufferSize() {
        return fileBufferSize;
    }

    public void setFileBufferSize(int fileBufferSize) {
        this.fileBufferSize = fileBufferSize;
    }
}

