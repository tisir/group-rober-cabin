package group.rober.common;

import group.rober.common.service.FileManageService;
import org.junit.Assert;
import org.junit.Test;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.sql.DataSource;

public class CommonStarterTest extends BaseTest {
    @Autowired
    protected SqlSessionTemplate sqlSessionTemplate;
    @Autowired
    @Qualifier(CommonConsts.IMG_FILE_SERVICE_NAME)
    protected FileManageService imgFileManageService;

    @Autowired
    protected DataSource dataSource;

    @Test
    public void test(){
        Assert.assertNotNull(dataSource);
        Assert.assertNotNull(imgFileManageService);
    }

    @Test
    public void baseAutoConfigTest(){
        Assert.assertNotNull(sqlSessionTemplate);
    }
}
