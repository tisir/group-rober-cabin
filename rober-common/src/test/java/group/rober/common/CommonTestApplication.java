package group.rober.common;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class CommonTestApplication {
    public static void main(String[] args){
        SpringApplication.run(CommonTestApplication.class,args);
    }
}
