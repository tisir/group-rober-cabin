package group.rober.sql.core;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.HashMap;
import java.util.Map;

public class AbstractUpdater {
    protected NamedParameterJdbcTemplate jdbcTemplate;

    public NamedParameterJdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int execute(String sql){
        return execute(sql,new HashMap<String, Object>());
    }
    public int execute(String sql,Map<String,?> paramMap){
        return jdbcTemplate.update(sql,paramMap);
    }

}
