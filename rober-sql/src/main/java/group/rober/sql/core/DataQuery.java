package group.rober.sql.core;

import group.rober.runtime.kit.*;
import group.rober.runtime.model.PaginationData;
import group.rober.sql.core.rowmapper.JpaBeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * SQL到JavaBean之间的查询映射
 */
public class DataQuery extends AbstractQuery {
    private ThreadLocal<RowMapper<?>> rowMapperThreadLocal = new ThreadLocal<RowMapper<?>>();



    public DataQuery() {
    }

    public DataQuery(NamedParameterJdbcTemplate jdbcOperations) {
        this.jdbcTemplate = jdbcOperations;
    }

    /**
     * 可以为每次查询，设置独立的RowMapper
     * @param rowMapper
     * @param <T>
     */
    public <T> void setRowMapperOnce(RowMapper<T> rowMapper){
        rowMapperThreadLocal.set(rowMapper);
    }

    /**
     * 为每次查询设置独立的RowMapper之后，这里可以获取一次（仅能调用一次，第二次调用，需要重新set)
     * @param classType
     * @param <T>
     * @return
     */
    public <T> RowMapper getRowMapperOnce(Class<T> classType){
        RowMapper rowMapper = rowMapperThreadLocal.get();
        if(rowMapper != null){
            //用完后，记得释放
            rowMapperThreadLocal.remove();
            return rowMapper;
        }
        rowMapper = new JpaBeanPropertyRowMapper<T>(classType);

        return rowMapper;
    }

    public <T> List<T> selectList(Class<T> classType, String sql, Map<String, ?> parameter){
        List<T> dataList = jdbcTemplate.query(sql,parameter,getRowMapperOnce(classType));
        return dataList;
    }

    public <T> List<T> selectList(Class<T> classType, String sql){
        return selectList(classType,sql,new HashMap<String, Object>());
    }

    public <T> List<T> selectList(Class<T> classType, String sql,String k1,Object v1){
        return selectList(classType,sql, MapKit.mapOf(k1,v1));
    }

    public <T> List<T> selectList(Class<T> classType, String sql,String k1,Object v1,String k2,Object v2){
        return selectList(classType,sql,MapKit.mapOf(k1,v1,k2,v2));
    }

    public <T> List<T> selectList(Class<T> classType, String sql,String k1,Object v1,String k2,Object v2,String k3,Object v3){
        return selectList(classType,sql,MapKit.mapOf(k1,v1,k2,v2,k3,v3));
    }

    public <T> T selectOne(Class<T> classType, String sql, Map<String, ?> parameter) {
        List<T> dataList = selectList(classType,sql,parameter);
        if(dataList!=null&&dataList.size()>0)return dataList.get(0);
        return null;
    }



    public <T> T selectOneById(Class<T> classType, Map<String,?> paraMap){
        StringBuffer sqlBuffer = new StringBuffer();
        sqlBuffer.append("SELECT * FROM ").append(JpaKit.getTableName(classType));
        sqlBuffer.append(" WHERE ");
        sqlBuffer.append(JpaKit.getIdWhere(classType));
        String sql = sqlBuffer.toString();

        return selectOne(classType,sql,paraMap);
    }

    private <T,V> Map<String,Object> buildIdValuesToMap(Class<T> classType,V... values){
        Map<String,String> columnPropertyMap = JpaKit.getIdMappedFields(classType);
        Map<String,Object> paramMap = new HashMap<String,Object>();
        int i=0;
        for(Map.Entry<String,String> entry : columnPropertyMap.entrySet()){
            Object v = null;
            if(i<values.length)v = values[i++];
            paramMap.put(entry.getValue(),v);
        }
        return paramMap;
    }

    public <T,V> T selectOneById(Class<T> classType, V... values){
        Map<String,Object> paramMap = buildIdValuesToMap(classType,values);
        return selectOneById(classType,paramMap);
    }

    public <T> boolean selectExistsById(Class<T> classType, Map<String,?> paraMap){
        StringBuffer sqlBuffer = new StringBuffer();
        sqlBuffer.append("SELECT count(1) FROM ").append(JpaKit.getTableName(classType));
        sqlBuffer.append(" WHERE ");
        sqlBuffer.append(JpaKit.getIdWhere(classType));
        String sql = sqlBuffer.toString();

        int r = jdbcTemplate.queryForObject(sql, paraMap, new RowMapper<Integer>() {
            public Integer mapRow(ResultSet rs, int index) throws SQLException {
                return rs.getInt(1);
            }
        });
        return r > 0;
    }

    public <T,V> boolean selectExistsById(Class<T> classType, V... values){
        Map<String,Object> paramMap = buildIdValuesToMap(classType,values);
        return selectExistsById(classType,paramMap);
    }


    public <T> T selectOne(Class<T> classType, String sql) {
        return selectOne(classType,sql,new HashMap<String, Object>());
    }

    public <T> T selectOne(Class<T> classType, String sql,String k1,Object v1) {
        return selectOne(classType,sql,MapKit.mapOf(k1,v1));
    }

    public <T> T selectOne(Class<T> classType, String sql,String k1,Object v1,String k2,Object v2) {
        return selectOne(classType,sql,MapKit.mapOf(k1,v1,k2,v2));
    }

    public <T> T selectOne(Class<T> classType, String sql,String k1,Object v1,String k2,Object v2,String k3,Object v3) {
        return selectOne(classType,sql,MapKit.mapOf(k1,v1,k2,v2,k3,v3));
    }

    public <T> PaginationData<T> selectListPagination(Class<T> classType, PaginationQuery query){
        return selectListPagination(query,getRowMapperOnce(classType));
    }

    public <T> PaginationData<T> selectListPagination(Class<T> classType, String sql, Map<String,?> paramMap, int index, int size){
        Map<String,Object> queryParamMap = new HashMap<String,Object>();
        queryParamMap.putAll(paramMap);

        PaginationQuery query = new PaginationQuery();
        query.setQuery(sql);
        query.setParameterMap(queryParamMap);
        query.setIndex(index);
        query.setSize(size);

        return selectListPagination(classType,query);
    }

}
