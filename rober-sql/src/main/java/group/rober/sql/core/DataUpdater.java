package group.rober.sql.core;

import group.rober.runtime.kit.JpaKit;
import group.rober.runtime.kit.ListKit;
import group.rober.runtime.kit.ValidateKit;
import group.rober.runtime.support.BeanSelfAware;
import group.rober.sql.kit.DataSQLKit;
import group.rober.sql.listener.DeleteListener;
import group.rober.sql.listener.InsertListener;
import group.rober.sql.listener.UpdateListener;
import org.springframework.aop.framework.AopContext;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.util.List;
import java.util.Map;

/**
 * SQL到JavaBean之间的写入映射
 */
public class DataUpdater extends AbstractUpdater implements BeanSelfAware  {
    private DataQuery dataQuery;

    /**
     * 获取代理对象自己，用于方法内部自我调用，无法AOP问题
     */
    private DataUpdater self;

    public void setSelf(BeanSelfAware self) {
        if(self instanceof DataUpdater) {
            this.self = (DataUpdater) self;
        }
    }
    public BeanSelfAware self() {
        return self;
    }

    public DataQuery setDataQuery() {
        return dataQuery;
    }

    public void setDataQuery(DataQuery dataQuery) {
        this.dataQuery = dataQuery;
    }

    public <T> int insert(T object, KeyHolder keyholder, InsertListener<T> listener){
        if(listener != null) listener.before(object);

        String sql = DataSQLKit.getInsertSql(object.getClass());
        SqlParameterSource sps = new BeanPropertySqlParameterSource(object);
        //执行
        int r = jdbcTemplate.update(sql,sps,keyholder);
        if(listener != null)listener.after(object);
        return 0;
    }
    public <T> int insert(T object, InsertListener<T> listener){
        return insert(object,new GeneratedKeyHolder(),listener);
    }
    public <T> int insert(T object){
        return insert(object,null);
    }

    public <T> int insert(List<T> objects, InsertListener<List<T>> listener){
        ValidateKit.notEmpty(objects,"要插入的数据列表不能为空");
        if(listener != null) listener.before(objects);

        String sql = DataSQLKit.getInsertSql(objects.get(0).getClass());

        SqlParameterSource[] spsList = new SqlParameterSource[objects.size()];
        for(int i=0;i<objects.size();i++){
            spsList[i] = new BeanPropertySqlParameterSource(objects.get(i));
        }
        //执行
        int ret = 0;
        int[] r = jdbcTemplate.batchUpdate(sql,spsList);
        for(int i=0;i<r.length;i++){
            ret += r[i];
        }

        if(listener != null)listener.after(objects);
        return 0;
    }
    public <T> int insert(List<T> objects){
        return insert(objects,null);
    }

    public <T> int update(T object,UpdateListener<T> listener){
        if(listener != null )listener.before(object);

        String sql = DataSQLKit.getUpdateSql(object.getClass());
        SqlParameterSource sps = new BeanPropertySqlParameterSource(object);
        int ret = jdbcTemplate.update(sql,sps);

        if(listener != null) listener.after(object);

        return ret;
    }

    public <T> int update(T object){
        return update(object,null);
    }
    public <T> int update(List<T> objects, UpdateListener<List<T>> listener){
        ValidateKit.notEmpty(objects,"要更新的数据列表不能为空");
        if(listener != null) listener.before(objects);

        String sql = DataSQLKit.getUpdateSql(objects.get(0).getClass());

        SqlParameterSource[] spsList = new SqlParameterSource[objects.size()];
        for(int i=0;i<objects.size();i++){
            spsList[i] = new BeanPropertySqlParameterSource(objects.get(i));
        }
        //执行
        int ret = 0;
        int[] r = jdbcTemplate.batchUpdate(sql,spsList);
        for(int i=0;i<r.length;i++){
            ret += r[i];
        }

        if(listener != null)listener.after(objects);
        return 0;
    }
    public <T> int update(List<T> objects){
        return update(objects,null);
    }

    /**
     * 根据业务主键保存，自动判别是插入还是更新
     * @param object
     * @param insertListener
     * @param updateListener
     * @param <T>
     * @return
     */
    public <T> int save(T object, InsertListener<T> insertListener, UpdateListener<T> updateListener){
        Map<String,Object> idMap = JpaKit.getIdMap(object);
        ValidateKit.notEmpty(idMap,"类[{0}]不存在@Id注解",object.getClass().getName());
        boolean exists = dataQuery.selectExistsById(object.getClass(),idMap);
        if(!exists){
            return self.insert(object,insertListener);
//            return insert(object,insertListener);
        }else{
            return self.update(object,updateListener);
//            return update(object,updateListener);
        }
    }
    public <T> int save(T object){
        return save(object,null,null);
    }

    /**
     * 根据业务主键保存，自动判别是插入还是更新
     * @param objects
     * @param insertListener
     * @param updateListener
     * @param <T>
     * @return
     */
    public <T> int save(List<T> objects,InsertListener<List<T>> insertListener, UpdateListener<List<T>> updateListener){
        List<T> insertDataList = ListKit.newArrayList();
        List<T> updateDataList = ListKit.newArrayList();

        for(T object : objects){
            Map<String,Object> idMap = JpaKit.getIdMap(object);
            boolean exists = dataQuery.selectExistsById(object.getClass(),idMap);
            if(exists){
                updateDataList.add(object);
            }else{
                insertDataList.add(object);
            }
        }

        int ret = 0;
        if(insertDataList.size()>0){
            ret += self.insert(insertDataList,insertListener);
        }
        if(updateDataList.size()>0){
            ret += self.update(updateDataList,updateListener);
        }

        return ret;
    }

    public <T> int save(List<T> objects){
        return save(objects,null,null);
    }

    public <T> int delete(T object, DeleteListener<T> listener){
        if(listener != null )listener.before(object);

        String sql = DataSQLKit.getDeleteSqlByKey(object.getClass());
        SqlParameterSource sps = new BeanPropertySqlParameterSource(object);
        int ret = jdbcTemplate.update(sql,sps);

        if(listener != null) listener.after(object);

        return ret;
    }

    public <T> int delete(T object){
        return delete(object,null);
    }

    public <T> int delete(List<T> objects,DeleteListener<List<T>> listener){
        if(listener != null )listener.before(objects);

        String sql = DataSQLKit.getDeleteSqlByKey(objects.get(0).getClass());

        SqlParameterSource[] spsList = new SqlParameterSource[objects.size()];
        for(int i=0;i<objects.size();i++){
            spsList[i] = new BeanPropertySqlParameterSource(objects.get(i));
        }
        //执行
        int ret = 0;
        int[] r = jdbcTemplate.batchUpdate(sql,spsList);
        for(int i=0;i<r.length;i++){
            ret += r[i];
        }

        if(listener != null) listener.after(objects);

        return ret;
    }

    public <T> int delete(List<T> objects){
        return delete(objects,null);
    }

}
