package group.rober.sql.autoconfigure;

import group.rober.sql.dialect.SqlDialectType;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "group.rober.sql", ignoreUnknownFields = true)
public class SqlProperties {
    private SqlDialectType sqlDialectType = SqlDialectType.MYSQL;

    public SqlDialectType getSqlDialectType() {
        return sqlDialectType;
    }

    public void setSqlDialectType(SqlDialectType sqlDialectType) {
        this.sqlDialectType = sqlDialectType;
    }
}
