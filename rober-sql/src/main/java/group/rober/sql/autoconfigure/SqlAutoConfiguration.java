package group.rober.sql.autoconfigure;

import group.rober.sql.core.*;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Map;

@EnableConfigurationProperties(SqlProperties.class)
@ComponentScan(basePackages = "group.rober.sql")
public class SqlAutoConfiguration {

    private SqlProperties properties;

    public SqlAutoConfiguration(SqlProperties properties) {
        this.properties = properties;
    }

    @Bean("dataQuery")
    @ConditionalOnClass(NamedParameterJdbcTemplate.class)
    @ConditionalOnMissingBean(DataQuery.class)
    public DataQuery getDataQuery(NamedParameterJdbcTemplate jdbcTemplate){
        DataQuery query = new DataQuery();

        query.setJdbcTemplate(jdbcTemplate);
        query.setSqlDialectType(properties.getSqlDialectType());

        return query;
    }

    @Bean("dataUpdater")
    @ConditionalOnClass(DataQuery.class)
    @ConditionalOnMissingBean(DataUpdater.class)
    public DataUpdater getDataUpdater(DataQuery dataQuery){
        DataUpdater updater = new DataUpdater();
        updater.setJdbcTemplate(dataQuery.getJdbcTemplate());
        updater.setDataQuery(dataQuery);
        return updater;
    }

    @Bean("mapDataQuery")
    @ConditionalOnClass(NamedParameterJdbcTemplate.class)
    @ConditionalOnMissingBean(MapDataQuery.class)
    public MapDataQuery getMapDataQuery(NamedParameterJdbcTemplate jdbcTemplate){
        MapDataQuery query = new MapDataQuery();

        query.setJdbcTemplate(jdbcTemplate);
        query.setSqlDialectType(properties.getSqlDialectType());

        return query;
    }
    @Bean("mapDataUpdater")
    @ConditionalOnClass(NamedParameterJdbcTemplate.class)
    @ConditionalOnMissingBean(MapDataUpdater.class)
    public MapDataUpdater getMapDataUpdater(NamedParameterJdbcTemplate jdbcTemplate,MapDataQuery mapDataQuery){
        MapDataUpdater updater = new MapDataUpdater();

        updater.setJdbcTemplate(jdbcTemplate);
        updater.setDataQuery(mapDataQuery);

        return updater;
    }

    @Bean("dataAccessor")
    @ConditionalOnClass({DataQuery.class,DataUpdater.class})
    @ConditionalOnMissingBean(DataAccessor.class)
    public DataAccessor getDataAccessor(DataQuery dataQuery,DataUpdater dataUpdater){
        DataAccessor dataAccessor = new DataAccessor(dataQuery,dataUpdater);
        return dataAccessor;
    }

    @Bean("mapDataAccessor")
    @ConditionalOnClass({MapDataQuery.class,MapDataUpdater.class})
    @ConditionalOnMissingBean(MapDataAccessor.class)
    public MapDataAccessor getMapDataAccessor(MapDataQuery mapDataQuery,MapDataUpdater mapDataUpdater){
        MapDataAccessor mapDataAccessor = new MapDataAccessor(mapDataQuery,mapDataUpdater);
        return mapDataAccessor;
    }
}
