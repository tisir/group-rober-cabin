package group.rober.sql.dialect;

import group.rober.sql.BaseTest;
import org.junit.Test;

import java.util.regex.Pattern;

public class SqlDialectTest {

//    @Test
    public void mysqlDialectTest(){
        String sql = "select * from DEMO_PERSON where CODE > :code";
        SqlDialect sqlDialect = new SqlMySqlDialect();
        String pagnationSql = sqlDialect.getPaginationSql(sql,0,12);
        System.out.print("MySQL:"+pagnationSql);
    }

    @Test
    public void oracleDialectTest(){
        String sql = "select * from DEMO_PERSON where CODE > :code";
        SqlDialect sqlDialect = new SqlOracleDialect();
        String pagnationSql = sqlDialect.getPaginationSql(sql,0,12);
        System.out.print("ORACLE:"+pagnationSql);
    }

//    @Test
    public void matchTest(){
        Pattern ALL_COLUMN_PATTERN = Pattern.compile("\\s*\\*\\s*");

        System.out.println(ALL_COLUMN_PATTERN.matcher("*").find());
        System.out.println(ALL_COLUMN_PATTERN.matcher(" * ").find());
        System.out.println(ALL_COLUMN_PATTERN.matcher(" .* ").find());
        System.out.println(ALL_COLUMN_PATTERN.matcher(" ABC.* ").find());
    }
}
