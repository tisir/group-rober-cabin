<#include "../views/macro/layout.ftl" />
<@layout title="401-错误的请求">
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="middle-box text-center animated fadeInDown">
            <h1>401</h1>
            <h3>用户名或密码错误</h3>
            <div>
                <a class="btn btn-link btn-lg" href="${request.contextPath}/" role="button">转到主页</a>
            </div>
        </div>
    </div>
</div>
</@layout>
