<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>出错了</title>
    <link rel="stylesheet" type="text/css" href="${ctxPath}/assets/css/404/main.css">
</head>
<body>
<div id="wrapper">
    <div id="main">
        <header id="header">
            <h1><span class="icon">!</span>404<span class="sub">资源不存在</span></h1>
        </header>
        <div id="content">
            <h2>${e.message}</h2>
            <p>
                当您看到这个页面,表示您的访问出错,这个错误是您打开的页面不存在,请确认您输入的地址是正确的,如果是在本站点击后出现这个页面,请联系站长进行处理,或者请通过下边的搜索重新查找资源,站长感谢您的支持!
            </p>
            <div class="utilities">
                <form name="formsearch" id="formkeyword">
                    <div class="input-container">
                        <input type="text" class="left" name="q" size="24" placeholder="别搜了，这个搜索框只是用来装饰的..."/>
                        <button id="search"></button>
                    </div>
                </form>
                <a class="button right" href="javascript:void(0);" onClick="history.go(-1);return true;">返回上页</a>
                <a class="button right" href="${ctxPath}">网站首页</a>
                <div class="clear"></div>
            </div>
        </div>
        <div id="footer">
            <div style="padding:20px 0;margin-top:30px;">
            </div>
        </div>
    </div>
</div>

</body>
</html>
