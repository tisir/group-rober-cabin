<#include "../views/macro/layout.ftl" />
<@layout title="405-不允许此方法 ">
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="middle-box text-center animated fadeInDown">
            <h1>405</h1>
            <h3> 不允许此方法 ！! !</h3>
            <div>
                抱歉，页面好像去火星啦！
            </div>
            <div>
                <pre>
                    对于请求所标识的资源，不允许使用请求行中所指定的方法。请确保为所请求的资源设置了正确的 MIME 类型。
                    如果问题依然存在，请与服务器的管理员联系
                </pre>
            </div>
            <div>
                <a class="btn btn-link btn-lg" href="${request.contextPath}/" role="button">转到主页</a>
            </div>
        </div>
    </div>
</div>
</@layout>