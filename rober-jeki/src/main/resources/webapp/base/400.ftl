<#include "../views/macro/layout.ftl" />
<@layout title="400-错误的请求">
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="middle-box text-center animated fadeInDown">
            <h1>400</h1>
            <h3>错误的请求</h3>
            <div>
                抱歉，你的请求出错啦！！！
            </div>
            <div>
                <pre>
                    由于语法格式有误，服务器无法理解此请求。不作修改，客户程序就无法重复此请求。
                </pre>
            </div>
            <div>
                <a class="btn btn-link btn-lg" href="${request.contextPath}/" role="button">转到主页</a>
            </div>
        </div>
    </div>
</div>
</@layout>