<#-- 页面布局使用到的宏 -->
<#macro layout title>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>${title}</title>
    <link rel="icon" type="image/x-icon" href="${request.contextPath}/favicon.ico">
    <link rel="stylesheet" href="${request.contextPath}/plugins/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="${request.contextPath}/plugins/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" href="${request.contextPath}/assets/css/icon.css">
    <link rel="stylesheet" href="${request.contextPath}/assets/css/index.css">
</head>
<body>
<#--<nav class="navbar navbar-default navbar-fixed-top" role="navigation">-->
<nav class="navbar navbar-default" role="navigation"  id="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="${request.contextPath}/"><span>JE</span><span>KI</span></span></a>
        </div>

        <ul class="nav navbar-nav navbar-left">
        <#--<li class="active"><a href="${request.contextPath}/domain/java">JAVA</a></li>-->
        <#--<li><a href="${request.contextPath}/domain/rax">Rax框架</a></li>-->
        <#--<li><a href="${request.contextPath}/domain/database">数据库</a></li>-->
        <#--<li><a href="${request.contextPath}/domain/osm">运维</a></li>-->
            <#list navDomainList as navDomain>
                <li <#if navDomain.code==activeDomain.code! >class="active"</#if> ><a href="${request.contextPath}/domain/${navDomain.code}"><i class="${navDomain.icon!}"></i><span>${navDomain.name}</span></a></li>
            </#list>

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <@shiro.authenticated>
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" onclick="dropdown(this)">
                        <i class="rober rober-user-tie"></i>&nbsp;
                        <span>您好,${sessionUser.name!}</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="${request.contextPath}/my/home"><i class="rober rober-user-tie"></i>&nbsp;<span>我的空间</span></a></li>
                        <li><a href="${request.contextPath}/admin/home"><i class="rober rober-users"></i>&nbsp;<span>管理员中心</span></a></li>
                        <li class="divider"></li>
                        <li><a href="${ctxPath}/auth/logoff?redirectLocation=/"><i class="rober rober-exit"></i>&nbsp;<span>退出登录</span></a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" onclick="dropdown(this)">
                        <i class="rober rober-quill" aria-hidden="true"></i>&nbsp;<span>写文章</span><b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="${request.contextPath}/article/create/markdown" target="_blank"><i class="fa fa-file-text-o"></i>&nbsp;<span>Markdown格式</span></a></li>
                        <li><a href="${request.contextPath}/article/create/richtext" target="_blank"><i class="fa fa-file-word-o"></i>&nbsp;<span>多媒体文本</span></a></li>
                        <li class="divider"></li>
                        <li><a href="${request.contextPath}/article/create/pdf" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;<span>PDF文档</span></a></li>
                    </ul>
                </li>
            </@shiro.authenticated>
            <@shiro.notAuthenticated>
                <li><a href="${request.contextPath}/register"><i class="rober rober-pagebreak"></i>&nbsp;<span>注册</span></a></li>
                <li><a href="${request.contextPath}/logon"><i class="rober rober-enter"></i> &nbsp;<span>登录</span></a></li>
            </@shiro.notAuthenticated>
        </ul>
        <#--<@shiro.authenticated>-->
            <#--<ul class="nav navbar-nav navbar-right">-->
                <#--<li class="dropdown">-->
                    <#--<a href="#" class="dropdown-toggle" data-toggle="dropdown">-->
                        <#--<i class="fa fa-pencil" aria-hidden="true"></i>写文章<b class="caret"></b>-->
                    <#--</a>-->
                    <#--<ul class="dropdown-menu">-->
                        <#--<li><a href="${request.contextPath}/article/create/markdown" target="_blank"><i class="fa fa-file-text-o"></i>Markdown格式</a></li>-->
                        <#--<li><a href="${request.contextPath}/article/create/richtext" target="_blank"><i class="fa fa-file-word-o"></i>多媒体文本</a></li>-->
                        <#--<li class="divider"></li>-->
                        <#--<li><a href="${request.contextPath}/article/create/pdf" target="_blank"><i class="fa fa-file-pdf-o"></i>PDF文档</a></li>-->
                    <#--</ul>-->
                <#--</li>-->
            <#--</ul>-->
        <#--</@shiro.authenticated>-->
    </div>
</nav>


<#nested>


<footer>
    <div class="container">
        <div class="row">
             <div class="col-sm-4">
                <h4>关于我们</h4>
                <p>Rax，A3Web以大资管，租赁，信贷等金融IT系统内部交流社区</p>
                 <p>本网站由&nbsp;<span class="glyphicon glyphicon-heart"></span>&nbsp;Rober社区制作</p>
            </div>

            <div class="col-sm-4">
                <h4>导航</h4>
                <ul class="unstyled">
                    <li><a href=""><span class="fa fa-tachometer"></span>主页</a></li>
                    <li><a href=""><span class="fa fa-server"></span>服务</a></li>
                    <li><a href=""><span class="fa fa-link"></span>链接</a></li>
                    <li><a href=""><span class="fa fa-send"></span>联系我们</a></li>
                </ul>
            </div>

            <div class="col-sm-4">
                <h4>关注我们</h4>
                <ul class="unstyled">
                    <li><a href=""><span class="fa fa-weibo"></span>微博</a></li>
                    <li><a href=""><span class="fa fa-wechat"></span>微信</a></li>
                    <li><a href=""><span class="fa fa-qq"></span>QQ</a></li>
                </ul>
            </div>

            <div class="col-sm-12">
                <h5 id="copyright">Copyright &copy;Rober</h5>
            </div>
        </div>
    </div>

</footer><!--页脚结束-->

</body>
</html>
<#-- 系统引入的前端资源 -->
<script src="${request.contextPath}/plugins/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>
<script src="${request.contextPath}/plugins/bootstrap/3.3.7/js/bootstrap.js" type="text/javascript"></script>
<script src="${request.contextPath}/plugins/bootstrap-paginator/1.0.2/bootstrap-paginator.js" type="text/javascript"></script>
<#-- rober相关核心资源 -->
<script src="${request.contextPath}/rober/rober-core.js" type="text/javascript"></script>
<script type="text/javascript">
    function dropdown (el) {
        $(el).next().toggle();
    }

    (function(){
        var copyrightDom = $('#copyright'), navbarDom = $('#navbar'),jumbotronDom = $('#jumbotron'),animationDom = $('#animation-bubble-wrapper');
        var toTopInit = copyrightDom.offset().top;
        var navHeight = navbarDom.outerHeight();
        $('body').scroll(function(event){
            var toTopCurr = copyrightDom.offset().top;
            if(toTopInit - toTopCurr <= navHeight){
                navbarDom.removeClass("nav-fixed");
                animationDom.remove();
                jumbotronDom.append(animationDom);
            }else{
                navbarDom.addClass("nav-fixed");
                animationDom.remove();
                navbarDom.append(animationDom);
            }
//        var scrollTop = document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop || window.parent.document.documentElement.scrollTop;
        })
    })()



</script>
</#macro>
