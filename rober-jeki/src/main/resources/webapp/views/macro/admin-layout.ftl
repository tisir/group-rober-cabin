<#macro adminLayout title>
    <#include "../macro/layout.ftl" />
    <@layout title="${title}">

    <div class="page-mylayout">
        <div class="nav-vertical">
            <div class="header">
                <a class="navbar-brand" href="${request.contextPath}/"><span>JE</span><span>KI</span></span></a>
            </div>
            <ul class="nav nav-pills nav-stacked">
                <li role="presentation" <#if activeSlideItem=="home" >class="active"</#if> ><a href="${request.contextPath}/admin/home"><i class="rober rober-home"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>主页</span></a></li>
                <li role="presentation" <#if activeSlideItem=="user" >class="active"</#if> ><a href="${request.contextPath}/admin/user-list"><i class="rober rober-users"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>所有用户</span></a></li>
                <li role="presentation" <#if activeSlideItem=="domain" >class="active"</#if> ><a href="${request.contextPath}/admin/domain-list"><i class="rober rober-tree"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>领域分类</span></a></li>
                <li role="presentation" <#if activeSlideItem=="tag" >class="active"</#if> ><a href="${request.contextPath}/admin/tag-list"><i class="rober rober-price-tags"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>标签</span></a></li>
                <li role="presentation" <#if activeSlideItem=="article" >class="active"</#if> ><a href="${request.contextPath}/admin/article-list"><i class="rober rober-list-numbered"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>全部文章</span></a></li>
                <li role="presentation" <#if activeSlideItem=="profile" >class="active"</#if> ><a href="${request.contextPath}/admin/profile"><i class="rober rober-database"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>系统配置</span></a></li>
            </ul>
        </div>
        <div class="setting-content">
            <#nested>
        </div>
    </div>

    </@layout>
</#macro>
