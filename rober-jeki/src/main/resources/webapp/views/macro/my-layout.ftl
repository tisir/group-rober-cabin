<#macro myLayout title>
    <#include "../macro/layout.ftl" />
    <@layout title="${title}">
    <div class="page-mylayout">
        <div class="nav-vertical">
            <div class="header">
                <a class="navbar-brand" href="${request.contextPath}/"><span>JE</span><span>KI</span></span></a>
            </div>
            <ul class="nav nav-pills nav-stacked">
                <li role="presentation"><a href="${request.contextPath}/my/home" ><i class="rober rober-home"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>主页</span></a></li>
                <li role="presentation"><a href="${request.contextPath}/my/article-list"><i class="rober rober-pen"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>文章</span></a></li>
                <li role="presentation"><a href="${request.contextPath}/my/profile"><i class="rober rober-cogs"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>个人资料修改</span></a></li>
            </ul>
        </div>
        <div class="setting-content">
            <#nested>
        </div>
    </div>
    </@layout>
</#macro>