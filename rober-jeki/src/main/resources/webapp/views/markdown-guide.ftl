<#include "macro/layout.ftl" />
<@layout title="markdown手册">
<link rel="stylesheet" href="${request.contextPath}/editor.md-1.5.0/css/editormd.min.css">
<div class="row">
<div id="content-editor">
    <textarea style="display: none" id="content">
    </textarea>
</div>
</@layout>
<script src="${request.contextPath}/editor.md-1.5.0/editormd.min.js" type="text/javascript"></script>
<script type="text/javascript">
    var editor;
    $(function() {
        $.get("${ctxPath}/editormd-guide.md",function(text){
            editor = editormd("content-editor", {
                markdown: text,
                width: "100%",
                height: 600,
                autoHeight : true,
                codeFold: true,
                htmlDecode: true,
                tex: true,
                taskList: true,
                emoji: true,
                flowChart: true,
                sequenceDiagram: true,
                onload:function(){
                    $(".CodeMirror").css("margin-top","78px");//解决前两行被隐藏问题
                },
                path: '${request.contextPath}/editor.md-1.5.0/lib/'
            });
        });
    });

</script>