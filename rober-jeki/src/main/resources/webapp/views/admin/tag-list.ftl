<#include "../macro/admin-layout.ftl" />
<@adminLayout title="标签管理">
<ol class="breadcrumb">
    <li>管理员中心</li>
    <li>标签管理</li>
</ol>
<div class="row admin-inner-row">
    <div class="col-sm-7">
        <h3><span>标签列表</span>&nbsp;&nbsp;<a href="${ctxPath}/admin/tag/statistics-action" class="btn btn-primary btn-sm">重新统计标签引用计数</a></h3>
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width:100px;">#</th>
                <th style="width:80px;">领域代码</th>
                <th>领域名称</th>
                <th style="width:80px;">排序号</th>
                <th style="width:120px;">管理</th>
            </tr>
            </thead>
            <tbody>
                <#list tagList as tag>
                <tr>
                    <td>${tag.id}</td>
                    <td>${tag.code!}</td>
                    <td class="flex-container"><a href="${ctxPath}/tag/${tag.code}" target="_blank">${tag.name!}</a><span class="badge">${tag.referCount}</span></td>
                    <td>${tag.sort!}</td>
                    <td>
                        <div class="btn-group btn-group-xs">
                            <a href="${ctxPath}/admin/tag/${tag.id}" class="btn btn-link"><i class="fa fa-pencil"></i>编辑</a>
                            <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="${ctxPath}/admin/tag/delete-action/${tag.id}"><i class="fa fa-trash-o"></i>删除</a></a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
                </#list>
            </tbody>
        </table>
    </div>
    <div class="col-sm-5" style="padding-left:20px;">
        <h3>快速创建</h3>
        <#include "tag-form.ftl" />
    </div>
</div>
</@adminLayout>
