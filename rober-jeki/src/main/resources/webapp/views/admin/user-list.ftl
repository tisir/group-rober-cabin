<#include "../macro/admin-layout.ftl" />
<@adminLayout title="文章列表">
    <ol class="breadcrumb">
        <li>管理员中心</li>
        <li>用户管理</li>
    </ol>

    <div class="row">
        <h3>我的主页</h3>
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width:100px;">#</th>
                <th style="width:150px;">用户名</th>
                <th style="width:200px;">昵称</th>
                <th>真实姓名</th>
                <th style="width:80px;">状态</th>
                <th style="width:120px;">管理</th>
            </tr>
            </thead>
            <tbody>
                <#list userList as user>
                <tr>
                    <th>${user.id}</th>
                    <td>${user.code}</td>
                    <td>${user.nick!}</td>
                    <td>${user.name}</td>
                    <td>${user.status!}</td>
                    <td>
                        <div class="btn-group btn-group-xs">
                            <button type="button" class="btn btn-link"><i class="fa fa-pencil"></i>修改</button>
                            <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#"><i class="fa fa-trash-o"></i>删除</a></a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
                </#list>
            </tbody>
        </table>
    </div>
</@adminLayout>
