<#include "../macro/admin-layout.ftl" />
<@adminLayout title="文章列表">
<ol class="breadcrumb">
    <li>管理员中心</li>
    <li>文章管理</li>
</ol>
<div class="row">
    <h3>文章管理</h3>
    <table class="table table-hover">
        <thead>
        <tr>
            <th style="width:60px;">#</th>
            <th>文章</th>
            <th style="width:80px;">评论/阅读</th>
            <th style="width:150px;">管理</th>
        </tr>
        </thead>
        <tbody>
            <#list articleList as article>
            <tr>
                <td>
                ${article.id}
                </td>
                <td>
                    <#if article.format == "markdown">
                        <i class="fa fa-trademark"></i>
                    <#elseif article.format == "richtext">
                        <i class="fa fa-file-word-o"></i>
                    <#elseif article.format == "pdf">
                        <i class="fa fa-file-pdf-o"></i>
                    <#else>
                        <i class="fa fa-exclamation-triangle"></i>
                    </#if>
                    <a href="${ctxPath}/article/${article.id}.html" target="_blank">${article.title!}</a>
                    <#if article.stick gt 0 ><i class="fa fa-heart"></i></#if>
                    <small class="text-right">${article.updatedTime!}@<@auth.user userId="${article.createdBy!}" /></small>
                    <span class="pull-right">${article.tags!}</span>
                </td>
                <td>${article.commentCount!}/${article.viewCount!}</td>
                <td>
                    <div class="btn-group btn-group-xs">
                        <a role="button" class="btn btn-link" href="${ctxPath}/article/editor/${article.id}" target="_blank"><i class="fa fa-pencil"></i>修改</a>
                        <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="${ctxPath}/admin/article/stick/${article.id}"><i class="fa fa-map-pin"></i>置顶</a></a></li>
                            <li><a href="${ctxPath}/admin/article/stick-cancel/${article.id}">取消置顶</a></a></li>
                            <li><a href="${ctxPath}/admin/article/delete/${article.id}"><i class="fa fa-trash-o"></i>删除</a></a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            </#list>
        </tbody>
    </table>
</div>

</@adminLayout>
