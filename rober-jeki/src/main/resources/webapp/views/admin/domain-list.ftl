<#include "../macro/admin-layout.ftl" />
<@adminLayout title="领域分类">
    <ol class="breadcrumb">
        <li>管理员中心</li>
        <li>领域分类</li>
    </ol>

    <div class="row">
        <h3><span>我的主页</span>&nbsp;&nbsp;<a href="${ctxPath}/admin/domain-create" class="btn btn-primary"><i class="fa fa-plus"></i>新建分类领域</a></h3>
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width:100px;">#</th>
                <th style="width:80px;">领域代码</th>
                <th>领域名称</th>
                <th style="width:80px;">排序号</th>
                <th style="width:80px;">状态</th>
                <th style="width:120px;">管理</th>
            </tr>
            </thead>
            <tbody>
                <#list domainList as domain>
                <tr>
                    <td>${domain.id}</td>
                    <td>${domain.code}</td>
                    <td><a href="${ctxPath}/admin/domain/${domain.id}">${domain.name!}</a></td>
                    <td>${domain.sort!}</td>
                    <td>${domain.status!}</td>
                    <td>
                        <div class="btn-group btn-group-xs">
                            <a href="${ctxPath}/admin/domain/${domain.id}" class="btn btn-link"><i class="fa fa-pencil"></i>编辑</a>
                            <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="${ctxPath}/admin/domain/delete-action/${domain.id}"><i class="fa fa-trash-o"></i>删除</a></a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
                </#list>
            </tbody>
        </table>
    </div>


</@adminLayout>
