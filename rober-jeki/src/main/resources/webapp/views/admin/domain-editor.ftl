<#include "../macro/admin-layout.ftl" />
<@adminLayout title="创建&编辑分类">
    <style type="text/css">
        .label{
            display: inline-block;
        }
    </style>
    <ol class="breadcrumb">
        <li>管理员中心</li>
        <li>创建&编辑分类</li>
    </ol>

<div class="row">
    <div class="col-sm-7">
        <h3>领域详情</h3>
        <form class="form-horizontal col-md-8 col-md-offset-2" action="${request.contextPath}/admin/domain/save-action" method="post">
            <div class="form-group">
                <label for="id" class="col-sm-2 control-label">ID</label>
                <div class="col-sm-10">
                    <span class="form-control" name="id" id="id">${domain.id!}</span>
                    <input type="hidden" name="id" id="id" value="${domain.id!}">
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">名称</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" id="name" value="${domain.name!}" placeholder="分类名称">
                </div>
            </div>
            <div class="form-group">
                <label for="code" class="col-sm-2 control-label">代码</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="code" id="code" value="${domain.code!}" placeholder="分类代码">
                </div>
            </div>
            <div class="form-group">
                <label for="sort" class="col-sm-2 control-label">排序值</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="sort" id="sort" value="${domain.sort!}" placeholder="排序值">
                </div>
            </div>
            <div class="form-group">
                <label for="intro" class="col-sm-2 control-label">简要描述</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="intro" id="intro" placeholder="分类简要描述">${domain.intro!}</textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="center-block btn btn-success"><i class="fa fa-paper-plane"></i>确定</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-sm-5">
        <div>
            <h3>已使用标签</h3>
            <#list inDomainTagList as tag>
                <span class="label label-success">${tag.name}<a href="${ctxPath}/admin/domain/${domain.id}/remove-tag/${tag.id}"><i class="fa fa-remove"></i></a></span>
            </#list>
        </div>
        <div>
            <h3 class="panel-heading">未使用标签</h3>
            <#list outDomainTagList as tag>
                <span class="label label-default">${tag.name!}<a href="${ctxPath}/admin/domain/${domain.id}/add-tag/${tag.id}"><i class="fa fa-plus"></i></a></span>
            </#list>
        </div>
    </div>
</div>

</@adminLayout>
