<#include "../macro/admin-layout.ftl" />
<@adminLayout title="我的主页">
    <ol class="breadcrumb">
        <li>管理员中心</li>
        <li>主页</li>
    </ol>
    <div>
        <div>
            <h3>我的主页</h3>
            <a href="javascript:void(0)" id="btn-rebuild-index" class="btn btn-primary btn-sm">重建全文索引</a>
        </div>
        <div>
            <div id="message-result" style="display: none;" class="alert alert-success" role="alert"></div>
        </div>
    </div>
</@adminLayout>
<script type="text/javascript">
    $(document).ready(function(){
        $("#btn-rebuild-index").click(function(){
            $.ajax({
                url:"${ctxPath}/admin/search/build-article-index",
                method:"post",
                success:function(){
                    $("#message-result").show().text("重建全文索引成功！");
                }
            });
        });
    });
    $.ajax()

</script>
