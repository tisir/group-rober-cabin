<form class="form-horizontal" action="${request.contextPath}/admin/tag/save-action" method="post">
    <div class="form-group">
        <label for="id" class="control-label">ID</label>
            <#--<input class="form-control" name="id" id="id">${tag.id!}-->
        <input type="text" class="form-control"  name="id" id="id" placeholder="标签ID" value="${tag.id!}">
    </div>
    <div class="form-group">
        <label for="code" class="control-label">代码</label>
        <input type="text" class="form-control" name="code" id="code" value="${tag.code!}" placeholder="标签代码">
    </div>
    <div class="form-group">
        <label for="name" class="control-label">名称</label>
        <input type="text" class="form-control" name="name" id="name" value="${tag.name!}" placeholder="标签名称">
    </div>
    <div class="form-group">
        <label for="sort" class="control-label">排序值</label>
        <input type="text" class="form-control" name="sort" id="sort" value="${tag.sort!}" placeholder="排序值">
    </div>
    <div class="form-group">
        <label for="intro" class="control-label">简要描述</label>
        <textarea class="form-control" name="intro" id="intro" placeholder="标签简要描述">${tag.intro!}</textarea>
    </div>
    <div class="form-group">
        <label for="icon" class="control-label">标签图标</label>
        <input type="text" class="form-control" name="icon" id="icon" value="${tag.icon!}" placeholder="标签图标">
    </div>

    <div class="form-group">
        <label for="submit" class="control-label"></label>
        <div>
            <button type="submit" name="submit" class="center-block btn btn-success"><i class="fa fa-paper-plane"></i>保存</button>
        </div>

    </div>
</form>