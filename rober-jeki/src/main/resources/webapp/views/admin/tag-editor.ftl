<#include "../macro/admin-layout.ftl" />
<@adminLayout title="编辑标签">
<ol class="breadcrumb">
    <li>管理员中心</li>
    <li>编辑标签</li>
</ol>
<#include "tag-form.ftl" />
</@adminLayout>
