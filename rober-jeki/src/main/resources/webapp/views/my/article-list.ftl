<#include "../macro/my-layout.ftl" />
<@myLayout title="文章列表">
    <ol class="breadcrumb">
        <li>个人中心</li>
        <li>文章管理</li>
    </ol>
    <div class="row">
        <h3>文章管理</h3>
        <table class="table table-hover">
            <thead>
            <tr>
                <th style="width:60px;">#</th>
                <th>文章</th>
                <th style="width:80px;">评论/阅读</th>
                <th style="width:150px;">管理</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">1</th>
                <td>Mark</td>
                <td>3/10</td>
                <td>
                    <div class="btn-group btn-group-xs">
                        <button type="button" class="btn btn-link"><i class="fa fa-pencil"></i>修改</button>
                        <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#"><i class="fa fa-trash-o"></i>删除</a></a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>Jacob</td>
                <td>3/10</td>
                <td>@fat</td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td>Larry</td>
                <td>3/10</td>
                <td>@twitter</td>
            </tr>
            </tbody>
        </table>
    </div>

</@myLayout>