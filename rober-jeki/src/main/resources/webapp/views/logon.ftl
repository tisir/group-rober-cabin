<#include "macro/layout.ftl" />
<@layout title="用户登录">
<div class="page-login">
    <form class="form-horizontal" action="${request.contextPath}/auth/logon" method="post">
        <div class="header form-group">
            <a class="navbar-brand" href="${request.contextPath}/"><span>JE</span><span>KI</span></span></a>
        </div>
        <div class="header form-group">
            <span>登录你的&nbsp;JEKI&nbsp;账户</span>
        </div>
        <div class="form-group">
            <label for="userCode" class="control-label">用户名</label>
            <input type="text" class="form-control" name="userCode" id="userCode" placeholder="用户名/电子邮件" value="">
        </div>
        <div class="form-group">
            <label for="password" class="control-label">密码</label>
            <input type="password" class="form-control" name="password" id="password" placeholder="密码" value="">
        </div>
        <div class="form-group">
            <input type="hidden" name="redirectLocation" id="redirectLocation" value="/" >
            <label class="control-label"></label>
            <button type="submit" class="btn btn-success">登录</button>
        </div>
    </form>
</div>
</@layout>
