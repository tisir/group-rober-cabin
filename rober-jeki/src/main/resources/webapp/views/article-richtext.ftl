<#include "${viewPath}/macro/layout.ftl" />
<@layout title="${article.title!}">
<div class="row">
    <div class="col-xs-12 col-sm-offset-1 col-sm-10 col-lg-offset-2 col-lg-8 page-article">
        <div class="page-header">
            <div class="title">${article.title!}
                <small>
                    <div class="btn-group btn-group-xs pull-right">
                        <a href="${ctxPath}/article/editor/${article.id}" class="btn btn-link btn-xs" role="button"><i
                                class="fa fa-pencil"></i>修改</a>
                        <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#" class="btn-warning"><i class="fa fa-map-pin"></i>置顶</a></a></li>
                        </ul>
                    </div>
                </small>
            </div>
        </div>
        <div class="header-sub">
            <p>
                <span class="author">作者:<@auth.user userId="${article.createdBy!}"></@auth.user></span>
                <span class="time">${article.createdTime!}</span>
                <span>
                    <#list tagNames as tagName>
                        <span class="tag">${tagName}</span>
                    </#list>
            </span>
                <span class="summary">${article.summary!}</span>
            </p>
            <p>
                <span class="reading">${article.viewCount!}阅读</span>
                <span class="comment">${article.commentCount!}评论</span>
            </p>
        </div>

    <#--这是文章正文内容-->
        <div class="row">
            <div class="col-md-12">
                <div id="editormd-view">
                ${article.content}
                </div>
            </div>
        </div>


        <hr>
        <p>
            <button class="btn comment-tag"><i class="rober rober-star-full store" aria-hidden="true"></i>收藏</button>
            <button class="btn comment-tag"><i class="rober rober-cool2 prise" aria-hidden="true"></i>顶</button>
            <button class="btn comment-tag"><i class="rober rober-evil2 criticize" aria-hidden="true"></i>踩</button>
        </p>


        <div class="article-list-wrapper">
            <div class="col-md-h">
                <div class="article-list-wrapper">
                    <div class="article-logo-container">
                        <span style="color: #cc0033" class="rober rober-link"></span>
                    </div>
                    <div class="article-list-container">
                        <div class="page-header">
                            <h4>关联文章</h4>
                        </div>
                        <ul class="list-group">
                            <li class="list-group-item"><a class="text-primary"
                                                           href="${request.contextPath}/article/1001">Oracle表空间相关操作</a>
                            </li>
                            <li class="list-group-item"><a class="text-primary"
                                                           href="${request.contextPath}/article/1002">列表搜索器搜索项自定义SQL查询条件是怎么解决的</a>
                            </li>
                            <li class="list-group-item"><a class="text-primary"
                                                           href="${request.contextPath}/article/1003">在显示模板的Handler处理类中，调整显示模板样式</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-h">
                <div class="article-list-wrapper">
                    <div class="article-logo-container">
                        <span style="color: #33cc33" class="rober rober-operation1"></span>
                    </div>
                    <div class="article-list-container">
                        <div class="page-header">
                            <h4>最新发布</h4>
                        </div>
                        <ul class="list-group">
                            <li class="list-group-item"><a class="text-primary"
                                                           href="${request.contextPath}/article/1001">Oracle表空间相关操作</a>
                            </li>
                            <li class="list-group-item"><a class="text-primary"
                                                           href="${request.contextPath}/article/1002">列表搜索器搜索项自定义SQL查询条件是怎么解决的</a>
                            </li>
                            <li class="list-group-item"><a class="text-primary"
                                                           href="${request.contextPath}/article/1003">在显示模板的Handler处理类中，调整显示模板样式</a>
                            </li>
                            <li class="list-group-item"><a class="text-primary"
                                                           href="${request.contextPath}/article/1006">RaxAnyApp参考指南.pdf</a>
                            </li>
                            <li class="list-group-item"><a class="text-primary"
                                                           href="${request.contextPath}/article/1007">PDF在HTML5插件实现在线查看</a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h4>评论(8)</h4>
                </div>
                <form action="${ctxPath}/article/add-comment/${article.id}" method="post">
                    <div class="form-group">
                    <textarea type="text" style="height:150px;" id="content" name="content"
                              class="form-control"></textarea>
                    </div>
                    <div class="pull-right">
                        <button class="right-block btn btn-success" type="submit">发表评论</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <ul class="media-list">
                    <#list commentList as comment>
                        <li class="media">
                            <div class="media-left">
                                <a href="#">
                                    <img class="media-object" data-src="holder.js/64x64" alt="64x64"
                                         src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNWQ5YmI3ODE4NyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1ZDliYjc4MTg3Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy40Njg3NSIgeT0iMzYuNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4="
                                         data-holder-rendered="true" style="width: 64px; height: 64px;">
                                </a>
                            </div>
                            <div class="media-body">
                                <small>1楼 2017/01/22 17:44</small>
                                <p>${comment.content}</p>
                            </div>
                            <div class="media-right">
                                <div style="width:40px;">
                                <span>
                                    <a href="${ctxPath}/article/delete-comment/${article.id}/${comment.id}"
                                       class="btn btn-link btn-xs" role="button">
                                        <i class="fa fa-trash-o"></i>删除
                                    </a>
                                </span>
                                </div>
                            </div>
                        </li>
                    </#list>
                </ul>
            </div>
        </div>
    </div>
</div>
</@layout>
