<#include "macro/layout.ftl" />
<@layout title="标签">

<div class="page-domain page-tag row">
    <div class="col-sm-12">
        <ul class="media-list">
            <#list articleList as article>
                <li class="media">
                    <div class="media-left">
                        <a href="#">
                            <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNWQ5YmI3ODE4NyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1ZDliYjc4MTg3Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy40Njg3NSIgeT0iMzYuNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4=" data-holder-rendered="true" style="width: 64px; height: 64px;">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading"><a href="${ctxPath}/article/${article.id}.html" target="_blank">${article.title!}</a></h4>
                        <p>
                            <span class="author">作者:<@auth.user userId="${article.createdBy!}"></@auth.user></span>
                            <span class="time">${article.createdTime}</span>
                        </p>
                        <p class="content">${article.snapshotContent!}</p>
                        <div class="section">
                            <p>
                                <span class="reading">${article.viewCount}阅读</span>
                                <span class="comment">${article.commentCount}评论</span>
                            </p>
                            <p class="share">
                                <b>分享到:</b>
                                <span><i class="fa fa-weixin"></i></span>
                                <span><i class="fa fa-weibo"></i></span>
                                <span><i class="fa fa-qq"></i></span>
                            </p>
                        </div>
                    </div>
                </li>
            </#list>
        </ul>
    </div>
</div>
</@layout>
