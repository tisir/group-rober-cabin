<#include "macro/layout.ftl" />
<@layout title="首页">
<div class="jumbotron page-top" id="jumbotron">
    <div class="jumbotron-content">
        <h2 style="display: inline-block; font-size: 40px">欢迎来到JEKI！</h2>
        <p style="display: inline-block">这是一个互动学习平台。</p>
        <div class="row">
            <#include "search-field.ftl" />
        </div>
        <#--<p><a class="btn btn-info btn-lg" role="button">-->
            <#--学习更多</a>-->
        <#--</p>-->
    </div>
    <div id="animation-bubble-wrapper" class="animation-canvas">
        <canvas id="animation-bubble-canvas"></canvas>
    </div>
    <#--<div id="animation-spark-wrapper" class="animation-canvas">-->
        <#--<canvas id="animation-spark-canvas"></canvas>-->
    <#--</div>-->
</div>
<div class="index-wrapper">
        <div class="row" >
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="row" style="display: flex;align-items: stretch;flex-wrap:wrap;">
                    <div class="col-md-a">

                        <div class="article-list-wrapper">
                            <div>
                                <span style="color: #cc3300;" class="rober rober-fire icon-patch"></span>
                            </div>
                            <div class="article-list-container">
                                <div class="page-header">
                                    <h4>推荐文章</h4>
                                </div>
                                <#list stickedArticleList as article>
                                    <div class="col-sm-4 col-md-4 col-lg-3 recommendation-container">
                                        <a class="recommendation" href="${ctxPath}/article/${article.id}.html" target="_blank">
                                            <h4>
                                                <strong>${article.title!}</strong>
                                            </h4>
                                            <p class="summary">${article.summary!}</p>
                                            <p class="content">${article.snapshotContentShort!}</p>
                                        </a>
                                    </div>
                                </#list>
                            </div>
                        </div>
                        <#--<div class="page-header">-->
                            <#--<h4>推荐文章</h4>-->
                        <#--</div>-->
                        <#--&lt;#&ndash;<div style="display: flex;flex-wrap:wrap;">&ndash;&gt;-->

                        <#--<#list stickedArticleList as article>-->
                            <#--<div class="col-sm-6 recommendation-container">-->
                                <#--<div class="recommendation" href="${ctxPath}/article/${article.id}.html">-->
                                    <#--<h4>-->
                                        <#--<strong>${article.title!}</strong>-->
                                    <#--</h4>-->
                                    <#--<span>${article.summary!}</span>-->
                                    <#--<p class="content">${article.snapshotContent!}</p>-->
                                    <#--<a class="btn btn-default" href="${ctxPath}/article/${article.id}.html"  target="_blank">查看</a>-->
                                <#--</div>-->
                            <#--</div>-->
                        <#--</#list>-->

                    </div>
                    <div class="col-md-h">
                        <div class="article-list-wrapper">
                            <div class="article-logo-container">
                                <span style="color: #33cc33" class="rober rober-operation1"></span>
                            </div>
                            <div class="article-list-container">
                                <div class="page-header">
                                    <h4>最新发布</h4>
                                </div>
                                <ul class="list-group">
                                    <#list recentArticleList as article>
                                        <li class="list-group-item"><a class="text-primary" href="${ctxPath}/article/${article.id}.html" target="_blank">${article.title!}</a></li>
                                    </#list>
                                </ul>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-h">
                        <div class="article-list-wrapper">
                            <div class="article-logo-container">
                                <span style="color:#CC3399" class="rober rober-document1"></span>
                            </div>
                            <div class="article-list-container">
                                <div class="page-header">
                                    <h4>标准文档
                                    <#--<small><a class="text-primary"  href="${ctxPath}/domain/official-document">更多...</a></small>-->
                                        <div class="pull-right">
                                            <#list officalDocTagList as tag>
                                                <small><a class="text-primary" href="${ctxPath}/tag/${tag.code!}">${tag.name!}</a></small>
                                            </#list>
                                        </div>
                                    </h4>
                                </div>
                                <ul class="list-group">
                                    <#list officalDocArticleList as article>
                                        <li class="list-group-item"><a class="text-primary"  href="${ctxPath}/article/${article.id}.html" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;${article.title!}</a></li>
                                    </#list>
                                </ul>
                            </div>
                        </div>

                    </div>

                    <#-- 第二行 -->

                    <div class="col-md-h">
                        <div class="article-list-wrapper">
                            <div class="article-logo-container">
                                <span style="color:#000000" class="rober rober-question4"></span>
                            </div>
                            <div class="article-list-container">
                                <div class="page-header">
                                    <h4>常见问题
                                        <div class="pull-right">
                                            <#list faqTagList as tag>
                                                <small><a class="text-primary"  href="${ctxPath}/tag/${tag.code!}">${tag.name!}</a></small>
                                            </#list>
                                        </div>
                                    </h4>
                                </div>
                                <ul class="list-group">
                                    <#list faqArticleList as article>
                                        <li class="list-group-item"><a class="text-primary"  href="${ctxPath}/article/${article.id}.html" target="_blank">${article.title!}</a></li>
                                    </#list>
                                </ul>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-h">
                        <div class="article-list-wrapper">
                            <div class="article-logo-container">
                                <span style="color:#660033"  class="rober rober-solution6"></span>
                            </div>
                            <div class="article-list-container">
                                <div class="page-header">
                                    <h4>问答交流
                                        <div class="pull-right">
                                            <#list snsTagList as tag>
                                                <small><a class="text-primary"  href="${ctxPath}/tag/${tag.code!}">${tag.name!}</a></small>
                                            </#list>
                                        </div>
                                    </h4>
                                </div>
                                <ul class="list-group">
                                    <#list snsArticleList as article>
                                        <li class="list-group-item"><a class="text-primary"  href="${ctxPath}/article/${article.id}.html" target="_blank">${article.title!}</a></li>
                                    </#list>
                                </ul>
                            </div>
                        </div>

                    </div>

                <#-- 第三行 -->
                    <div class="col-md-h">
                        <div class="article-list-wrapper">
                            <div class="article-logo-container">
                                <span style="color:#33CCCC" class="rober rober-operation4"></span>
                            </div>
                            <div class="article-list-container">
                                <div class="page-header">
                                    <h4>业务方案
                                        <div class="pull-right">
                                            <#list bzsoTagList as tag>
                                                <small><a class="text-primary"  href="${ctxPath}/tag/${tag.code!}">${tag.name!}</a></small>
                                            </#list>
                                        </div>
                                    </h4>
                                </div>
                                <ul class="list-group">
                                    <#list bzsoArticleList as article>
                                        <li class="list-group-item"><a class="text-primary"  href="${ctxPath}/article/${article.id}.html" target="_blank">${article.title!}</a></li>
                                    </#list>
                                </ul>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-h">
                        <div class="article-list-wrapper">
                            <div class="article-logo-container">
                                <span style="color:#6699FF" class="rober rober-google-drive"></span>
                            </div>
                            <div class="article-list-container">
                                <div class="page-header">
                                    <h4>标签</h4>
                                </div>
                                <ul class="list-group">
                                    <#list allTagList as tag>
                                        <a class="tag"  href="${ctxPath}/tag/${tag.code!}" target="_blank">${tag.name}<span class="badge">${tag.referCount}</span></a>
                                    </#list>
                                </ul>
                            </div>
                        </div>

                    </div>

                <#-- 第四行 -->

                    <div class="col-md-h">
                        <div class="article-list-wrapper">
                            <div class="article-logo-container">
                                <span style="color:#CC3300" class="rober rober-java3"></span>
                            </div>
                            <div class="article-list-container">
                                <div class="page-header">
                                    <h4>JAVA
                                        <div class="pull-right">
                                            <#list javaTagList as tag>
                                                <small><a class="text-primary"  href="${ctxPath}/tag/${tag.code!}">${tag.name!}</a></small>
                                            </#list>
                                        </div>
                                    </h4>
                                </div>
                                <ul class="list-group">
                                    <#list javaArticleList as article>
                                        <li class="list-group-item"><a class="text-primary"  href="${ctxPath}/article/${article.id}.html" target="_blank">${article.title!}</a></li>
                                    </#list>
                                </ul>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-h">
                        <div class="article-list-wrapper">
                            <div class="article-logo-container">
                                <span style="color:#336666" class="rober rober-database1"></span>
                            </div>
                            <div class="article-list-container">
                                <div class="page-header">
                                    <h4>数据库
                                        <div class="pull-right">
                                            <#list dbTagList as tag>
                                                <small><a class="text-primary"  href="${ctxPath}/tag/${tag.code!}">${tag.name!}</a></small>
                                            </#list>
                                        </div>
                                    </h4>
                                </div>
                                <ul class="list-group">
                                    <#list dbArticleList as article>
                                        <li class="list-group-item"><a class="text-primary"  href="${ctxPath}/article/${article.id}.html" target="_blank">${article.title!}</a></li>
                                    </#list>
                                </ul>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <div class="col-md-3  col-sm-12 col-xs-12 tease-container">
                <form action="${ctxPath}/tweet/send" method="post" id="tweet-form">
                    <div class="form-inline">
                        <textarea type="text" class="form-control" id="content" name="content" placeholder="弹点什么..."></textarea>
                        <#--&nbsp;-->
                        <button class="btn btn-primary" onclick="javascript:$('#tweet-form').submit();">
                            发射
                        </button>
                    </div>

                    <#--<div class="input-group input-group-lg">-->
                        <#--<textarea ></textarea>-->
                        <#--<button class="input-group-addon" </button>-->
                        <#--&lt;#&ndash;<a class="" role="button" >发射</a>&ndash;&gt;-->
                    <#--</div>-->
                </form>
                <div class="list-group">
                    <#list tweetList as tweet>
                    <div class="media">
                        <div class="media-body">
                            <div class="flex-container">
                                <p>
                                    <img class="media-object" data-src="holder.js/24x24" alt="24x24" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNWQ5YmI3ODE4NyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1ZDliYjc4MTg3Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy40Njg3NSIgeT0iMzYuNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4=" data-holder-rendered="true" style="width: 24px; height: 24px;">
                                    <span>author</span>
                                </p>
                                <p class="created-time">${tweet.createdTime!}</p>
                            </div>

                            <p class="content">${tweet.content!}</p>
                            <a href="#" class="praise"><i class="rober rober-wink"></i>&nbsp;<span>赞+10</span></a>
                        </div>
                        <#--<p class="list-group-item-text">${tweet.createdTime!}</p>-->
                    </div>
                    </#list>
                    <#--<a href="#" class="list-group-item">-->
                        <#--<p class="list-group-item-text">我是个脸盲，我根本不知道她漂亮不漂亮</p>-->
                    <#--</a>-->
                    <#--<a href="#" class="list-group-item">-->
                        <#--<p class="list-group-item-text">我最后悔的就是创建了阿里巴巴</p>-->
                    <#--</a>-->
                    <#--<a href="#" class="list-group-item">-->
                        <#--<p class="list-group-item-text">我出生在一个普通家庭</p>-->
                    <#--</a>-->
                    <#--<a href="#" class="list-group-item">-->
                        <#--<p class="list-group-item-text">我有一个梦想</p>-->
                    <#--</a>-->
                </div>

            </div>
        </div>
    </div>
</@layout>
<#--<script src="${request.contextPath}/assets/js/TweenLite.min.js" type="text/javascript"></script>-->
<#--<script src="${request.contextPath}/assets/js/EasePack.min.js" type="text/javascript"></script>-->
<script src="${request.contextPath}/assets/js/bg-animation.js" type="text/javascript"></script>
<#--<script src="${request.contextPath}/assets/js/bg-animation2.js" type="text/javascript"></script>-->
