<#include "macro/layout.ftl" />
<@layout title="${article.title!}-编辑">
<link rel="stylesheet" href="${request.contextPath}/editor.md-1.5.0/css/editormd.min.css">
<link rel="stylesheet" href="${request.contextPath}/bootstrap-tagsinput-0.8.0/bootstrap-tagsinput-typeahead.css">
<link rel="stylesheet" href="${request.contextPath}/bootstrap-tagsinput-0.8.0/bootstrap-tagsinput.css">
<div class="page-article page-article-editor">
    <div class="page-header">
        <div class="title">编辑文章</div>
    </div>
    <form class="form-horizontal" action="${ctxPath}/article/save-action" method="post">
        <input type="hidden" id="id" name="id" value="${article.id!}">
        <input type="hidden" id="format" name="format" value="${article.format!}">
        <div class="form-group">
            <label for="title" class="control-label">标题</label>
            <input type="text" class="form-control" id="title" name="title" placeholder="请填写文章标题" value="${article.title!}"/>
        </div>
        <div class="form-group">
            <label for="summary" class="control-label">摘要</label>
            <textarea class="form-control" id="summary" name="summary" placeholder="文章简要介绍">${article.summary!}</textarea>
        </div>
        <hr>
        <div class="form-group">
            <div id="content-editor">
                <textarea class="form-control" style="display: none" id="content" name="content" placeholder="文章内容">${article.content!}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="tags" class="control-label">标签</label>
            <input type="text" class="form-control" data-role="tagsinput" id="tags" name="tags" value="${article.tags!}" placeholder="请输入标签">
        </div>
        <div class="form-group">
            <label for="tags" class="control-label">标签分类规则</label>
            <div>
                <dl class="dl-horizontal">
                <#list domainTagsList as domainTags>
                    <dt>${domainTags.domain.name}</dt>
                    <dd>
                    <#list domainTags.tags as tagItem >
                        <span class="tag">${tagItem.name}</span>
                    </#list>
                    </dd>
                </#list>
                </dl>
            </div>
        </div>
        <div class="form-group">
            <input type="checkbox" id="commentAble" name="commentAble" <#if article.commentAble! == "Y" >checked="checked"</#if> value="Y">
            &nbsp;
            <label for="allowComment" class="control-label">允许评论</label>
        </div>

        <div class="form-group">
            <button type="submit" class="center-block btn btn-lg btn-success"><i class="fa fa-paper-plane-o"></i>发布</button>
        </div>

    </form>
</div>
</@layout>
<script src="${request.contextPath}/editor.md-1.5.0/editormd.min.js" type="text/javascript"></script>
<script src="${request.contextPath}/typeahead.js-0.11.1/typeahead.bundle.js" type="text/javascript"></script>
<script src="${request.contextPath}/bootstrap-tagsinput-0.8.0/bootstrap-tagsinput.js" type="text/javascript"></script>
<#--<script src="http://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>-->
<#--<script src="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>-->
<#--<script src="http://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js" type="text/javascript"></script>-->

<script type="text/javascript">
    var editor;
    $(function() {
        var text = $("#content").text()||"\r\n\r\n\r\n\r\n\r\n\r\n\r\n";
        editor = editormd("content-editor", {
            markdown: text,
            width: "100%",
            height: 600,
            syncScrolling : "single",
            toolbarAutoFixed:true,
            autoHeight : true,
            codeFold: true,
            htmlDecode: true,
            tex: true,
            taskList: true,
            emoji: false,
            flowChart: true,
            sequenceDiagram: true,
            onload:function(){
                $(".CodeMirror").css("margin-top","78px");//解决前两行被隐藏问题
                <#--$.get("${ctxPath}/markdown-demo.md",function(text){-->
                    <#--editor.setMarkdown(text);-->
                <#--});-->
            },
            path: '${request.contextPath}/editor.md-1.5.0/lib/',
            imageUpload    : true,
            imageFormats   : ["jpg", "jpeg", "gif", "png", "bmp", "webp"],
            imageUploadURL : "${ctxPath}/article//upload-file",
        });

        //$('#tags');
        //${ctxPath}/mock/citynames.json
    });


</script>