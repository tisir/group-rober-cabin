<#include "../views/macro/layout.ftl" />
<@layout title="出错了">
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="middle-box text-center animated fadeInDown">
            <h3>服务器内部错误</h3>
            <div>
                抱歉，出错了！！！
            </div>
            <div>
                <pre>
                    ${e}
                </pre>
            </div>
            <div>
                <a class="btn btn-link btn-lg" href="${request.contextPath}/" role="button">转到主页</a>
            </div>
        </div>
    </div>
</div>
</@layout>
