<#include "macro/layout.ftl" />
<@layout title="用户注册成功">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <p class="text-center text-success"><i class="fa fa-check-circle-o" style="font-size: 20em;"></i></p>
            <p class="text-center text-danger">用户注册成功</p>
            <p class="text-center"><a class="btn btn-primary btn-lg" href="${request.contextPath}/logon" role="button">登录</a></p>
        </div>
    </div>
</@layout>
