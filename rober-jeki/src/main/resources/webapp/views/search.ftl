<#include "macro/layout.ftl" />
<@layout title="首页">
<div class="jumbotron page-top" id="jumbotron">
    <#--<div>-->
        <h1>欢迎来到JEKI！</h1>
        <p>这是一个互动学习平台。</p>
        <#--<p><a class="btn btn-info btn-lg" role="button">-->
            <#--学习更多</a>-->
        <#--</p>-->
    <#--</div>-->
    <div id="animation-bubble-wrapper" class="animation-canvas">
        <canvas id="animation-bubble-canvas"></canvas>
    </div>
    <#--<div id="animation-spark-wrapper" class="animation-canvas">-->
        <#--<canvas id="animation-spark-canvas"></canvas>-->
    <#--</div>-->
</div>
<div class="index-wrapper">
        <div class="row">
            <div class="row">
                <#include "search-field.ftl" />
            </div>
        </div>
        <div class="row" >
            <p>总共找到<mark>${hitsCount}</mark>条记录</p>
            <ul class="tab-content media-list">
                <#list artciles as article>
                    <li class="media">
                        <div class="media-body">
                            <h4 class="media-heading"><a href="${ctxPath}/article/${article.id}.html" target="_blank">${article.hlTitle!"没有标题"}</a></h4>
                            <p class="content">${article.hlContent!}</p>
                        </div>
                    </li>
                </#list>
            </ul>
        </div>
    </div>
</@layout>
