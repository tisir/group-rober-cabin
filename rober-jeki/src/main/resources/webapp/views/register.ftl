<#include "macro/layout.ftl" />
<@layout title="新用户注册">
<div class="page-register">
    <form class="form-horizontal" action="${request.contextPath}/auth/register" method="post">
        <div class="header form-group">
            <a class="navbar-brand" href="${request.contextPath}/"><span>JE</span><span>KI</span></span></a>
        </div>
        <div class="header form-group">
            <span>注册你的&nbsp;JEKI&nbsp;账户</span>
        </div>

        <div class="form-group">
            <label for="code" class="control-label">用户名</label>
            <input type="text" class="form-control" name="code" id="code" value="" placeholder="用户名">
        </div>
        <div class="form-group">
            <label for="nick" class="control-label">昵称</label>
            <input type="text" class="form-control" name="nick" id="nick" value="" placeholder="昵称">
        </div>
        <div class="form-group">
            <label for="name" class="control-label">真实姓名</label>
            <input type="text" class="form-control" name="name" id="name" value="" placeholder="真实姓名">
        </div>
        <div class="form-group">
            <label for="email" class="control-label">邮箱地址</label>
            <input type="email" class="form-control" name="email" id="email" value="" placeholder="邮箱地址">
        </div>
        <div class="form-group">
            <label for="password" class="control-label">密码</label>
            <input type="password" class="form-control" name="password" id="password" value="" placeholder="密码">
        </div>
        <div class="form-group">
            <label for="password2" class="control-label">确认密码</label>
            <input type="password" class="form-control" id="password2" placeholder="确认密码">
        </div>
        <div class="form-group">
            <label class="control-label"></label>
            <p>
                <input type="checkbox">
                <span>阅读并接受《用户协议》</span>
            </p>
        </div>
        <div class="form-group">
            <label class="control-label"></label>
            <button type="submit" class="btn btn-success">提交注册</button>
            <input type="hidden" name="redirectLocation" value="/base/redirect/views/register-success" >
        </div>

    </form>
</div>
</@layout>
