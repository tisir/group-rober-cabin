<#include "macro/layout.ftl" />
<@layout title="JEKI-${activeDomain.name!}">

    <div class="page-domain row">
        <div class="col-xs-12 col-sm-offset-1 col-sm-10">
            <div class="jumbotron">
                <span>${activeDomain.name!}</span>
                <h2>${activeDomain.intro!}</h2>
            </div>
            <ul class="nav nav-tabs">
                <#--<li role="presentation" class="active" data-toggle="tab"><a href="">全部</a></li>-->
                <#list tagList as tag>
                <li role="presentation" data-toggle="tab" <#if activeTag.code! == tag.code > class="active" </#if> onclick="tabToggle(event)" >
                    <a href="${ctxPath}/domain-tag/${activeDomain.code}/${tag.code!}">${tag.name!}</a>
                </li>
                </#list>
            </ul>

            <ul class="tab-content media-list">
                <#list articleList as article>
                    <li class="media">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNWQ5YmI3ODE4NyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1ZDliYjc4MTg3Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy40Njg3NSIgeT0iMzYuNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4=" data-holder-rendered="true" style="width: 64px; height: 64px;">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><a href="${ctxPath}/article/${article.id}.html" title="${article.title!}" target="_blank">${article.title!}</a></h4>

                            <#--<p class="content">${article.snapshotContent!?html}</p>-->
                            <p class="content">${briefPlainText(article.content!'',300)}...</p>
                            <p>
                                <span class="author"><@auth.user userId="${article.createdBy!}">&nbsp;发布于&nbsp;</@auth.user></span>
                                <span class="time">${article.createdTime}</span>
                                <span class="reading">${article.viewCount}阅读</span>
                                <span class="comment">${article.commentCount}评论</span>
                            </p>
                            <#--<div class="section">-->
                                <#--<p>-->

                                <#--</p>-->
                                <#--&lt;#&ndash;<p class="share">&ndash;&gt;-->
                                    <#--&lt;#&ndash;<b>分享到:</b>&ndash;&gt;-->
                                    <#--&lt;#&ndash;<span><i class="fa fa-weixin"></i></span>&ndash;&gt;-->
                                    <#--&lt;#&ndash;<span><i class="fa fa-weibo"></i></span>&ndash;&gt;-->
                                    <#--&lt;#&ndash;<span><i class="fa fa-qq"></i></span>&ndash;&gt;-->
                                <#--&lt;#&ndash;</p>&ndash;&gt;-->
                            <#--</div>-->
                        </div>
                    </li>
                </#list>
            </ul>
            <ul id='bp-element'></ul>

        </div>
    </div>
</@layout>
<script>
    function tabToggle(event){
        window.open(event.target.href,'_self');
//        console.log($(event.target))
    }

    $(function(){

        var element = $('#bp-element');
        options = {
            bootstrapMajorVersion:3, //对应的bootstrap版本
            size: 'large',
            alignment:'center',
            currentPage: ${paginationArticle.index +1 }, //设置当前页
            numberOfPages: 10, //设置控件显示的页码数.页码标签操作按钮的数量(最多这么多个，多了不显示）
            totalPages:${paginationArticle.pageCount}, //设置总页数
            shouldShowPage:${(paginationArticle.pageCount gt 1)?string("true","false")},//是否显示该按钮
            itemTexts: function (type, page, current) {//设置显示的样式，默认是箭头
                switch (type) {
                    case "first":
                        return "首页";
                    case "prev":
                        return "上页";
                    case "next":
                        return "下页";
                    case "last":
                        return "末页";
                    case "page":
                        return page;
                }
            },
            pageUrl: function(type, page, current){
                var urlObject = rober.parseURL();
                urlObject.param['pageIndex']=page-1;
                urlObject.param['pageSize']=${paginationArticle.size};

                return urlObject.path+'?'+rober.param(urlObject.param);

            }
        };
        element.bootstrapPaginator(options);



        console.log(rober.param(rober.parseURL('http://127.0.0.1:8080/jeki/domain/java?page=1&size=5&size=6')));
        console.log(rober.parseURL());


    });
</script>
