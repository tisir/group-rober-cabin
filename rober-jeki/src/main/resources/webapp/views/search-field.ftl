<form class="form-horizontal" id="searchForm" action="${ctxPath}/search" method="get">
    <div class="form-group">
        <div class="col-sm-8">
            <div class="input-group" style="max-width: 600px; margin: 0 auto">
                <input type="hidden" name="page" value="0">
                <input type="hidden" name="size" value="20">
                <input type="text" class="form-control input-lg" name="keyWord" id="keyWord" value="${keyWord!}">
                <span type="submit" onclick="searchForm.submit()" id="search-form-button" class="input-group-addon btn btn-primary"><i class="fa fa-search"></i>搜索</span>
            </div>
        </div>
    </div>
</form>
