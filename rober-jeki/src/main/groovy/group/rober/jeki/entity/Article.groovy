package group.rober.jeki.entity

import group.rober.runtime.kit.StringKit

import javax.persistence.Id
import javax.persistence.Table
import javax.persistence.Transient

/**
 * 文章实体，Groovy的形式
 */
@Table(name = "JEKI_ARTICLE")
class Article {
    @Id
    String id;
    String parentId;
    String domainCode;
    String title;
    String tags;
    String authorId;
    String summary;
    String content;
    String picture;
    String keyWords;
    long commentCount;
    String commentAble;
    long viewCount;
    String format;
    String status;
    String type;
    long goodCount;
    long badCount;
    long watchCount;
    double score;
    long stick;
    String perfectAble;
    String clientIp;
    String clientUserAgent;
    String clientLocation;
    long revision;
    String createdBy;
    Date createdTime;
    String updatedBy;
    Date updatedTime;

    @Transient
    public String getSnapshotContent(){
        if(StringKit.isBlank(content))return "！！！文章没有内容！！！";
        String snapshot = "";
        int maxLen = 300;
        if(content.length()>maxLen){
            snapshot = content.substring(0,maxLen)+"...";
        }
        return snapshot;
    }
}
