package group.rober.jeki.entity

import javax.persistence.Id
import javax.persistence.Table;


@Table(name = "JEKI_COMMENT")
class Comment {
    String articleId;
    @Id
    String id;
    String referId;
    String type;
    String content;
    String commentor;
    long anonymous;
    long referCount;
    double score;
    long goodCount;
    long badCount;
    String clientIp;
    String clientUserAgent;
    String clientLocation;
    long revision;
    String createdBy;
    Date createdTime;
    String updatedBy;
    Date updatedTime;
}
