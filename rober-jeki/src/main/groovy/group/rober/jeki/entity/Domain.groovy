package group.rober.jeki.entity

import javax.persistence.Id
import javax.persistence.Table;

@Table(name = "JEKI_DOMAIN")
class Domain implements Serializable, Cloneable {
    @Id
    String id;
    String code;
    String name;
    String intro;
    long sort;
    String icon;
    String type;
    long level;
    String url;
    String status;
    String seoTitle;
    String seoKeyWords;
    long revision;
    String createdBy;
    Date createdTime;
    String updatedBy;
    Date updatedTime;
}
