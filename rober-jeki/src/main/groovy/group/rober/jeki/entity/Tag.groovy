package group.rober.jeki.entity

import javax.persistence.Id
import javax.persistence.Table

@Table(name = "JEKI_TAG")
class Tag implements Serializable, Cloneable {
    @Id
    String id;
    String code;
    String name;
    String intro;
    long sort;
    long referCount;
    String icon;
    String seoTitle;
    String seoKeyWords;
    long watchCount;
    long revision;
    String createdBy;
    Date createdTime;
    String updatedBy;
    Date updatedTime;
}
