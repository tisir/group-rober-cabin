package group.rober.jeki.service;

import group.rober.jeki.entity.Tag;
import group.rober.runtime.kit.MapKit;
import group.rober.sql.core.DataAccessor;
import group.rober.sql.core.MapDataAccessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class TagService {
    @Autowired
    private DataAccessor dataAccessor;
    @Autowired
    private MapDataAccessor mapDataAccessor;


    public List<Tag> getTagList(){
        Map<String,Object> vars = MapKit.newHashMap();
        List<Tag> tagList = dataAccessor.selectList(Tag.class,"select * from JEKI_TAG where 1=1 order by SORT asc",vars);
        return tagList;
    }
    public Tag getTag(String id){
        Map<String,?> vars = MapKit.mapOf("id",id);
        return dataAccessor.selectOne(Tag.class,"select * from JEKI_TAG where ID=:id",vars);
    }
    public Tag getTagByCode(String id){
        Map<String,?> vars = MapKit.mapOf("code",id);
        return dataAccessor.selectOne(Tag.class,"select * from JEKI_TAG where CODE=:code",vars);
    }
    public Tag getTagByName(String name){
        Map<String,?> vars = MapKit.mapOf("name",name);
        return dataAccessor.selectOne(Tag.class,"select * from JEKI_TAG where NAME=:name",vars);
    }
    public boolean tagExistsByName(String name){
        Map<String,?> vars = MapKit.mapOf("name",name);
        return dataAccessor.selectCount("select count(1) from JEKI_TAG where NAME=:name",vars)>0;
    }

    public Tag saveTag(Tag tag){
        dataAccessor.save(tag);
        return tag;
    }
    public boolean deleteTag(Tag tag){
        dataAccessor.delete(tag);
        return true;
    }
}
