package group.rober.jeki.service;

import group.rober.jeki.entity.Domain;
import group.rober.jeki.entity.DomainTags;
import group.rober.jeki.entity.Tag;
import group.rober.runtime.kit.MapKit;
import group.rober.runtime.kit.NumberKit;
import group.rober.runtime.lang.MapData;
import group.rober.sql.core.DataAccessor;
import group.rober.sql.core.MapDataAccessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class DomainService {
    @Autowired
    private DataAccessor dataAccessor;
    @Autowired
    private MapDataAccessor mapDataAccessor;


    public List<Domain> getDomainList(){
        Map<String,Object> vars = MapKit.newHashMap();
        List<Domain> domainList = dataAccessor.selectList(Domain.class,"select * from JEKI_DOMAIN where 1=1 order by SORT desc",vars);
        return domainList;
    }
    public Domain getDomain(String id){
        Map<String,?> vars = MapKit.mapOf("id",id);
        return dataAccessor.selectOne(Domain.class,"select * from JEKI_DOMAIN where ID=:id",vars);
    }
    public Domain getDomainByCode(String id){
        Map<String,?> vars = MapKit.mapOf("code",id);
        return dataAccessor.selectOne(Domain.class,"select * from JEKI_DOMAIN where CODE=:code",vars);
    }
    public Domain saveDomain(Domain domain){
        dataAccessor.save(domain);
        return domain;
    }
    public boolean deleteDomain(Domain domain){
        dataAccessor.delete(domain);
        return true;
    }
    public void addTagToDomain(String tagId,String domainId){
        MapData row = new MapData();
        row.put("id", NumberKit.nanoTime36());
        row.put("tagId", tagId);
        row.put("domainId", domainId);
        mapDataAccessor.insert("JEKI_DOMAIN_TAG",row);
    }
    public void removeTagToDomain(String tagId,String domainId){
        MapData row = new MapData();
        row.put("tagId", tagId);
        row.put("domainId", domainId);
        mapDataAccessor.insert("JEKI_DOMAIN_TAG",row);
    }
    public List<Tag> getInDomainTagList(String domainId){
        return getInDomainTagList(domainId,-1);
    }
    public List<Tag> getInDomainTagList(String domainId, int limit){
        String sql = "select * from JEKI_TAG T " +
                " where exists(select 1 from JEKI_DOMAIN_TAG DT where T.ID=DT.TAG_ID and DT.DOMAIN_ID=:domainId) " +
                " order by SORT ASC,ID ASC";
        if(limit>0)sql += " LIMIT "+limit;
        return dataAccessor.selectList(Tag.class,sql, MapKit.mapOf("domainId",domainId));
    }
    public List<Tag> getOutDomainTagList(String domainId){
        String sql = "select * from JEKI_TAG T " +
                " where not exists(select 1 from JEKI_DOMAIN_TAG DT where T.ID=DT.TAG_ID and DT.DOMAIN_ID=:domainId) " +
                " order by SORT ASC,ID ASC";
        return dataAccessor.selectList(Tag.class,sql, MapKit.mapOf("domainId",domainId));
    }
    public List<DomainTags> getAllDomainTags(){
        List<DomainTags> domainTagsList = new ArrayList<DomainTags>();

        List<Domain> domainList = getDomainList();
        for(Domain domain:domainList){
            DomainTags domainTags = new DomainTags();
            domainTags.setDomain(domain);
            List<Tag> tagList = getInDomainTagList(domain.getId());
            domainTags.setTags(tagList);
            domainTagsList.add(domainTags);
        }

        return domainTagsList;
    }
}
