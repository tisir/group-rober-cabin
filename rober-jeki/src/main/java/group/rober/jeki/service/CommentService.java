package group.rober.jeki.service;

import group.rober.jeki.entity.Comment;
import group.rober.runtime.kit.MapKit;
import group.rober.sql.core.DataAccessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {
    private String defaultSortClause = " GOOD_COUNT DESC,BAD_COUNT ASC ,SCORE DESC ,ID ASC";
    @Autowired
    private DataAccessor dataAccessor;


    public void addComment(Comment comment){
        dataAccessor.insert(comment);
    }

    public void deleteComment(String commentId){
        dataAccessor.execute("delete from JEKI_COMMENT where ID=:id", MapKit.mapOf("id",commentId));
    }

    public List<Comment> getCommentListByArticle(String articleId){
        String sql = "select * from JEKI_COMMENT where TYPE='ArticleComment' and ARTICLE_ID=:articleId ORDER BY"+defaultSortClause;
        List<Comment> commentList = dataAccessor.selectList(Comment.class,sql, MapKit.mapOf("articleId",articleId));
        return commentList;
    }

    public List<Comment> getTweetList(){
        String sql = "select * from JEKI_COMMENT where TYPE='Tweet' ORDER BY UPDATED_TIME DESC";
        List<Comment> commentList = dataAccessor.selectList(Comment.class,sql);
        return commentList;
    }

}
