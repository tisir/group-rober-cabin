package group.rober.jeki.exception;

import group.rober.runtime.lang.RoberException;

public class JekiException extends RoberException {
    public JekiException() {
    }

    public JekiException(String message) {
        super(message);
    }

    public JekiException(String messageFormat, Object... objects) {
        super(messageFormat, objects);
    }

    public JekiException(Throwable cause, String messageFormat, Object... objects) {
        super(cause, messageFormat, objects);
    }

    public JekiException(Throwable cause, String message) {
        super(cause, message);
    }

    public JekiException(Throwable cause) {
        super(cause);
    }

    public JekiException(String message, Throwable cause) {
        super(message, cause);
    }

    public JekiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
