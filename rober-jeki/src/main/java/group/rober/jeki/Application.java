package group.rober.jeki;

import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;

@SpringBootApplication
//@EnableAspectJAutoProxy
@EnableAspectJAutoProxy(proxyTargetClass = true) //开启AspectJ代理，并将proxyTargetClass置为true，表示启用cglib对Class也进行代理
public class Application {
    public static void main(String[] args){
        SpringApplication.run(Application.class,args);
    }

}
