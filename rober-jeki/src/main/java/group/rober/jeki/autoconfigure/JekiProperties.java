package group.rober.jeki.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "group.rober.jeki", ignoreUnknownFields = true)
public class JekiProperties {
}
