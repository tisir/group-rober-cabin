package group.rober.jeki.autoconfigure;

import group.rober.base.interceptor.WebHolderInterceptor;
import group.rober.jeki.interceptor.AdminSliderNavInterceptor;
import group.rober.jeki.interceptor.NavHeaderInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.freemarker.FreeMarkerView;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Configuration
@ImportResource("classpath:application-context.xml")
@EnableConfigurationProperties(JekiProperties.class)
@ComponentScan(basePackages = "group.rober.jeki")   //要加上这句，否则此模块被其他模块引用时，自动@Component注解的类无法使用
public class JekiAutoConfiguration extends WebMvcConfigurerAdapter {
    protected JekiProperties properties;

    public JekiAutoConfiguration(JekiProperties properties) {
        this.properties = properties;
    }

    @Autowired
    protected NavHeaderInterceptor navHeaderInterceptor;
    @Autowired
    protected AdminSliderNavInterceptor adminSliderNavInterceptor;

    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(navHeaderInterceptor).addPathPatterns("/**");
        registry.addInterceptor(adminSliderNavInterceptor).addPathPatterns("/admin/**");
    }
}

