package group.rober.jeki.controller;

import group.rober.jeki.entity.Comment;
import group.rober.jeki.service.CommentService;
import group.rober.runtime.kit.NumberKit;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Api(description = "动弹")
@Controller
@RequestMapping("/tweet")
public class TweetController {
    @Autowired
    protected CommentService commentService;

    @RequestMapping(path="/send",method = RequestMethod.POST)
    public ModelAndView sendTweet(@RequestParam("content") String content){
        Comment comment = new Comment();
        comment.setId(NumberKit.nanoTime36());
        comment.setContent(content);
        comment.setType("Tweet");

        commentService.addComment(comment);

        return new ModelAndView("redirect:/");
    }
}
