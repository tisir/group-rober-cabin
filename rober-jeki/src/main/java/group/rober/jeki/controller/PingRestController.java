package group.rober.jeki.controller;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by tisir<yangsong158@qq.com> on 2017-04-13
 */

@Api(description = "页面基本测试")
@RestController
@RequestMapping("/ping")
public class PingRestController {
    @RequestMapping("/rest")
    public String helloWord(){
        return "HELLO SPRING BOOT IS RUNNING 4";
    }
    @RequestMapping("/wild-card/**")
    public String wildCard(HttpServletRequest request){
        System.out.println(request.getRequestURI());
        System.out.println(request.getServletPath());
        return "WILD CARD IS HERE:";
    }
}
