package group.rober.jeki.controller.admin;

import group.rober.jeki.entity.Domain;
import group.rober.jeki.entity.Tag;
import group.rober.jeki.service.DomainService;
import group.rober.runtime.kit.NumberKit;
import group.rober.runtime.kit.StringKit;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(description = "后台管理-分类")
@Controller
@RequestMapping("/admin")
public class AdminDomainController {
    @Autowired
    DomainService domainService;

    @RequestMapping("/domain-list")
    ModelAndView domainTag() {
        List<Domain> domainList = domainService.getDomainList();
        Map<String,Object> vars = new HashMap<String,Object>();
        vars.put("domainList",domainList);
        vars.put("activeSlideItem","domain");
        return new ModelAndView("views/admin/domain-list",vars);
    }

    @RequestMapping("/domain-create")
    ModelAndView domainCreate() {
        Domain domain = new Domain();
        Map<String,Object> vars = new HashMap<String,Object>();
        vars.put("domain",domain);
        return new ModelAndView("views/admin/domain-editor",vars);
    }
    @RequestMapping("/domain/{domainId}")
    ModelAndView domainEditor(@PathVariable("domainId") String domainId) {
        Domain domain = domainService.getDomain(domainId);
        List<Tag> inDomainTagList = domainService.getInDomainTagList(domainId);
        List<Tag> outDomainTagList = domainService.getOutDomainTagList(domainId);

        Map<String,Object> vars = new HashMap<String,Object>();
        vars.put("domain",domain);
        vars.put("inDomainTagList",inDomainTagList);
        vars.put("outDomainTagList",outDomainTagList);
        return new ModelAndView("views/admin/domain-editor",vars);
    }
    @RequestMapping("/domain/delete-action/{domainId}")
    ModelAndView domainDelete(@PathVariable("domainId") String domainId) {
        Domain domain = domainService.getDomain(domainId);
        domainService.deleteDomain(domain);
        return new ModelAndView("redirect:/admin/domain-list");
    }
    @RequestMapping(path="/domain/save-action",method = RequestMethod.POST)
    ModelAndView domainSaveAction(@ModelAttribute Domain dataDomain) {
        if(StringKit.isBlank(dataDomain.getId())){
            dataDomain.setId(NumberKit.nanoTime36());
        }
        domainService.saveDomain(dataDomain);
        return new ModelAndView("redirect:/admin/domain-list");
    }

    @RequestMapping("/domain/{domainId}/add-tag/{tagId}")
    ModelAndView domainAddTag(@PathVariable("domainId") String domainId,@PathVariable("tagId") String tagId) {
        domainService.addTagToDomain(tagId,domainId);
        return new ModelAndView("redirect:/admin/domain/"+domainId);
    }
    @RequestMapping("/domain/{domainId}/remove-tag/{tagId}")
    ModelAndView domainRemoveTag(@PathVariable("domainId") String domainId,@PathVariable("tagId") String tagId) {
        domainService.removeTagToDomain(tagId,domainId);
        return new ModelAndView("redirect:/admin/domain/{domainId}");
    }
}
