package group.rober.jeki.controller;

import io.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@Api(description = "个人中心")
@Controller
@RequestMapping("/my")
public class MyHomeController {
    @RequestMapping("/home")
    ModelAndView home() {
        return new ModelAndView("views/my/home");
    }
    @RequestMapping("/article-list")
    ModelAndView articleList() {
        Map<String,Object> vars = new HashMap<String,Object>();

        return new ModelAndView("views/my/article-list",vars);
    }
    @RequestMapping("/profile")
    ModelAndView profile() {
        Map<String,Object> vars = new HashMap<String,Object>();

        return new ModelAndView("views/my/profile",vars);
    }
}
