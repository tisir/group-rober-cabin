package group.rober.jeki.controller.admin;

import group.rober.auth.entity.User;
import group.rober.auth.service.AuthService;
import group.rober.jeki.service.DomainService;
import group.rober.runtime.kit.MapKit;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(description = "后台管理")
@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    AuthService authService;
    @Autowired
    DomainService domainService;



    @RequestMapping("/home")
    ModelAndView home() {
        return new ModelAndView("views/admin/home");
    }
    @RequestMapping("/profile")
    ModelAndView profile() {
        Map<String,Object> vars = new HashMap<String,Object>();
        return new ModelAndView("views/admin/profile",vars);
    }
    @RequestMapping("/user-list")
    ModelAndView userList() {
        List<User> userList = authService.selectUserList("", MapKit.newEmptyMap());
        Map<String,Object> vars = new HashMap<String,Object>();
        vars.put("userList",userList);
        return new ModelAndView("views/admin/user-list",vars);
    }
}
