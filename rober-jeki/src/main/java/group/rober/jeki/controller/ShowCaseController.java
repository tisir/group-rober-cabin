package group.rober.jeki.controller;

import io.swagger.annotations.Api;
import org.springframework.boot.SpringBootVersion;
import org.springframework.core.SpringVersion;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * 一些训练项目放这里来
 * Created by tisir<yangsong158@qq.com> on 2017-05-21
 */
@Api(description = "案例集合")
@Controller
public class ShowCaseController {

    @RequestMapping("/showcase/mvvm")
    public ModelAndView mvvm() {
        Map<String,Object> vars = new HashMap<String,Object>();

        vars.put("javaVersion", System.getProperty("java.version"));
        vars.put("springVersion", SpringVersion.getVersion());
        vars.put("springBootVersion", SpringBootVersion.getVersion());

        return new ModelAndView("showcase/mvvm",vars);
    }
}
