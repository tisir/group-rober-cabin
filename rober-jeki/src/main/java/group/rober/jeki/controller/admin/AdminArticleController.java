package group.rober.jeki.controller.admin;

import group.rober.jeki.entity.Article;
import group.rober.jeki.service.ArticleService;
import group.rober.jeki.service.CommentService;
import group.rober.runtime.holder.WebHolder;
import group.rober.runtime.model.PaginationData;
import group.rober.search.service.SearchService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@Api(description = "后台管理-文章")
@Controller
@RequestMapping("/admin")
public class AdminArticleController {
    @Autowired
    ArticleService articleService;
    @Autowired
    CommentService commentService;
    @Autowired
    SearchService searchService;

    @GetMapping("/article-list")
    ModelAndView articleList() {
        Map<String,Object> vars = new HashMap<String,Object>();
        Integer pageIndex = WebHolder.getRequestParameterForInt("pageIndex",0);
        Integer pageSize = WebHolder.getRequestParameterForInt("pageSize",10);

        PaginationData<Article> pdArticle = articleService.getArticleList(pageIndex,pageSize);

        vars.put("articleList",pdArticle.getDataList());
        vars.put("paginationArticle",pdArticle);

        return new ModelAndView("views/admin/article-list",vars);
    }
    @GetMapping("/article/delete/{articleId}")
    ModelAndView deleteArticle(@PathVariable("articleId") String articleId) {
        articleService.deleteArticle(articleId);
        return new ModelAndView("redirect:/admin/article-list");
    }
    @GetMapping("/article/stick/{articleId}")
    ModelAndView stickArticle(@PathVariable("articleId") String articleId) {
        articleService.stickArticle(articleId);
        return new ModelAndView("redirect:/admin/article-list");
    }
    @GetMapping("/article/stick-cancel/{articleId}")
    ModelAndView stickCancelArticle(@PathVariable("articleId") String articleId) {
        articleService.cancelArticle(articleId);
        return new ModelAndView("redirect:/admin/article-list");
    }
}
