package group.rober.jeki.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 管理菜单左右导航当前激活的导航项数据填充
 */
@Component
public class AdminSliderNavInterceptor implements HandlerInterceptor {
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        return true;
    }

    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        if(modelAndView==null)return;
        String uri = request.getRequestURI();
        String startUri = request.getServletContext().getContextPath()+"/admin";
        String suffixUri = uri.substring(startUri.length()+1).split("/")[0];
        String sliderName = suffixUri.split("-")[0];
        modelAndView.getModelMap().put("activeSlideItem",sliderName);
//        System.out.println("activeSlideItem:"+sliderName);
//        System.out.println("RequestURI:"+request.getRequestURI());
    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
