package group.rober.jeki.interceptor;

import group.rober.jeki.entity.Domain;
import group.rober.jeki.service.DomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 头部导航数据，通过拦截器放入
 */
@Component
public class NavHeaderInterceptor implements HandlerInterceptor {
    @Autowired
    DomainService domainService;

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        return true;
    }

    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        List<Domain> domainList = domainService.getDomainList();
        Map<String,Object> vars = new HashMap<String,Object>();
        if(modelAndView!=null){
            modelAndView.getModelMap().put("navDomainList",domainList);
            if(!modelAndView.getModelMap().containsKey("activeDomain")){
                modelAndView.getModelMap().put("activeDomain",new Domain());
            }
        }
    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
