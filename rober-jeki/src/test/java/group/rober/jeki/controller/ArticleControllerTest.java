package group.rober.jeki.controller;

import group.rober.jeki.BaseTest;
import group.rober.jeki.entity.Article;
import group.rober.runtime.kit.JSONKit;
import group.rober.runtime.kit.NumberKit;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ArticleControllerTest extends BaseTest {

    @Test
    public void saveArticleTest() throws Exception {
        Article article = new Article();
        article.setId("JUT"+ NumberKit.nanoTime36());
        article.setTitle("测试文章");
        article.setSummary("测试摘要");
        article.setContent("测试内容");
        String content = JSONKit.toJsonString(article);
        //构建请求对象
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/article/save-action")
                .contentType(MediaType.APPLICATION_JSON)
                .param("id","JUT"+ NumberKit.nanoTime36())
                .param("title","测试文章")
//                .content(content)
                ;

        MvcResult result = mockMvc.perform(requestBuilder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().is3xxRedirection())
                .andReturn();

    }
}
