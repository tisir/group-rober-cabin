package group.rober.jeki;

import group.rober.auth.AuthConsts;
import group.rober.jeki.controller.admin.AdminArticleController;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class JekiSearchTest extends BaseTest {

    @Test
    public void testBuildAndSearch() throws Exception {

        //构建请求对象
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/admin/article/build-search-index");

        MvcResult result = mockMvc.perform(requestBuilder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn();



        //构建请求对象
        requestBuilder = MockMvcRequestBuilders.get("/search")
        .param("keyWord","java")
        .param("page","0")
        .param("size","15")
        ;

        result = mockMvc.perform(requestBuilder)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andReturn();
    }
}
