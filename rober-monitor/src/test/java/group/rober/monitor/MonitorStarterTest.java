package group.rober.monitor;

import group.rober.sql.core.DataAccessor;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class MonitorStarterTest extends BaseTest{
    @Autowired
    protected JdbcTemplate jdbcTemplate;
    @Autowired
    protected DataAccessor dataAccessor;

    @Test
    public void runtimeTest(){
        Assert.assertNotNull(jdbcTemplate);
        Assert.assertNotNull(dataAccessor);
    }

}
