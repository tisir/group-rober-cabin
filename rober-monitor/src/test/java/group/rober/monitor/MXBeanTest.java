package group.rober.monitor;

import group.rober.runtime.kit.BeanKit;
import org.junit.Test;
import sun.management.ManagementFactoryHelper;

import java.lang.management.OperatingSystemMXBean;

public class MXBeanTest {

    @Test
    public void test01(){
        OperatingSystemMXBean osBean = ManagementFactoryHelper.getOperatingSystemMXBean();
        System.out.println(BeanKit.getPropertyValue(osBean,"systemCpuLoad"));
        System.out.println(BeanKit.getPropertyValue(osBean,"processCpuLoad"));
        long freePhysicalMemorySize = (long)BeanKit.getPropertyValue(osBean,"freePhysicalMemorySize");
        long totalPhysicalMemorySize = (long)BeanKit.getPropertyValue(osBean,"totalPhysicalMemorySize");
        System.out.println(freePhysicalMemorySize/1024.0/1024.0+"M");
        System.out.println(totalPhysicalMemorySize/1024.0/1024.0/1024.0+"G");
    }
}
