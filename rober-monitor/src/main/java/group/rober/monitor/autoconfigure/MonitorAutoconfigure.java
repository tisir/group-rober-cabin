package group.rober.monitor.autoconfigure;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@EnableConfigurationProperties(MonitorProperites.class)
@ComponentScan(basePackages = "group.rober.monitor")   //要加上这句，否则此模块被其他模块引用时，自动@Component注解的类无法使用
public class MonitorAutoconfigure {
}
