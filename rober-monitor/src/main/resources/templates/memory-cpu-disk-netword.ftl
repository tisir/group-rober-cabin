<!DOCTYPE HTML>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>[CPU-内存-磁盘-网络]监控</title>
    <link rel="stylesheet" href="${ctxPath}/plugins/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" href="${ctxPath}/plugins/font-awesome/4.6.3/css/font-awesome.min.css">
    <script src="${ctxPath}/plugins/jquery/1.12.4/jquery.min.js" type="text/javascript"></script>
    <script src="${ctxPath}/plugins/bootstrap/3.3.7/js/bootstrap.js" type="text/javascript"></script>
    <script src="${ctxPath}/plugins/echart/3.7.1/echarts.min.js" type="text/javascript"></script>
    <style type="text/css">
        body {
            padding-top: 50px;
        }

        .chart-now,.chart-his {
            height: 200px;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <nav class="navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Rober监控中心</a>
        </div>
        <div>
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">首页看板</a></li>
                <li><a href="#">URL映射</a></li>
                <li><a href="#">应用基本指标</a></li>
                <li><a href="#">自动配置</a></li>
                <li><a href="#">BEANS</a></li>
                <li><a href="#">环境变量</a></li>
                <li><a href="#">健康指标</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        更多<b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">追踪信息</a></li>
                        <li><a href="#">线程栈</a></li>
                        <li class="divider"></li>
                        <li><a href="#">配置属性</a></li>
                        <li><a href="#">应用信息</a></li>
                        <li class="divider"></li>
                        <li><a href="#">关闭应用</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <h3>内存<small class="text-success"><span id="data-memfree"></span>/<span id="data-mem"></span></small></h3>
    <div class="row metric-row">
        <div class="col-md-3 chart-now" id="memory-now">当前</div>
        <div class="col-md-9 chart-his" id="memory-his">历史明细</div>
    </div>
    <h3>CPU</h3>
    <div class="row metric-row">
        <div class="col-md-3">当前</div>
        <div class="col-md-9">历史明细</div>
    </div>
    <h3>磁盘</h3>
    <div class="row metric-row">
        <div class="col-md-3">当前</div>
        <div class="col-md-9">历史明细</div>
    </div>
    <h3>网卡</h3>
    <div class="row">
        <div class="col-md-3">当前</div>
        <div class="col-md-9">历史明细</div>
    </div>
</div>
<script type="text/javascript">
    //-----------------------
    //1.内存指标
    //-----------------------
    var gaugeOption = {
        series: [
            {
                type: 'gauge',
                detail: {formatter:'{value}%'},
                axisLine: {            // 坐标轴线
                    lineStyle: {       // 属性lineStyle控制线条样式
                        width: 5
                    }
                },
                axisTick: {            // 坐标轴小标记
                    length: 10,        // 属性length控制线长
                    lineStyle: {       // 属性lineStyle控制线条样式
                        color: 'auto'
                    }
                },
                splitLine: {           // 分隔线
                    length: 10,         // 属性length控制线长
                    lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
                        color: 'auto'
                    }
                },
                detail : {
                    // 其余属性默认使用全局文本样式，详见TEXTSTYLE
                    formatter: function (value) {
                        return value+"%"
                    },
                    fontWeight: 'bolder',
                    borderRadius: 3,
                    backgroundColor: '#444',
                    borderColor: '#aaa',
                    shadowBlur: 5,
                    shadowColor: '#333',
                    shadowOffsetX: 0,
                    shadowOffsetY: 3,
                    borderWidth: 0,
                    textBorderColor: '#000',
                    textBorderWidth: 1,
                    textShadowBlur: 2,
                    textShadowColor: '#fff',
                    textShadowOffsetX: 0,
                    textShadowOffsetY: 0,
                    fontFamily: 'Arial',
                    width: 10,
                    color: '#eee',
                    fontSize:12,
                    rich: {}
                },
                data: [{value: 0}]
            }
        ]
    };

    var memoryNow = $("#memory-now").get(0);
    var memoryHis = $("#memory-his").get(0);
    var memoryNowChart = echarts.init(memoryNow);

    var memoryNowOption = $.extend(true, {}, gaugeOption);


    //定时器
    setInterval(function (){
        $.get("${ctxPath}/metrics.json",function(data){
            if(!data)return;
            var mem = data["mem"];
            var memFree = data["mem.free"];
            memoryNowOption.series[0].data[0].value = ((memFree/mem)*100).toFixed(0);
            $("#data-memfree").text(parseInt(memFree/1024)+"M");
            $("#data-mem").text(parseInt(mem/1024)+"M");
            memoryNowChart.setOption(memoryNowOption);
        });
    },1000);

    memoryNowChart.setOption(memoryNowOption);

</script>
</body>
</html>

