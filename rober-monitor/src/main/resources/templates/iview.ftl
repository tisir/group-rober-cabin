<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>iview example</title>
    <link rel="stylesheet" type="text/css" href="${ctxPath}/plugins/iview/2.3.2/css/iview.css">
    <script type="text/javascript" src="${ctxPath}/plugins/vue/2.2.2/vue.min.js"></script>
    <script type="text/javascript" src="${ctxPath}/plugins/iview/2.3.2/js/iview.min.js"></script>
</head>
<body>
<div id="app">
    <i-button @click="show">Click me!</i-button>
    <Modal v-model="visible" title="Welcome">欢迎使用 iView</Modal>
</div>
<script>
    new Vue({
        el: '#app',
        data: {
            visible: false
        },
        methods: {
            show: function () {
                this.visible = true;
            }
        }
    })
</script>
</body>
</html>
