<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>iview nav</title>
    <link rel="stylesheet" type="text/css" href="${ctxPath}/plugins/iview/2.3.2/css/iview.css">
    <script type="text/javascript" src="${ctxPath}/plugins/vue/2.2.2/vue.min.js"></script>
    <script type="text/javascript" src="${ctxPath}/plugins/iview/2.3.2/js/iview.min.js"></script>
    <style type="text/css">
        .layout {
            border: 1px solid #d7dde4;
            background: #f5f7f9;
        }

        .layout-logo {
            width: 100px;
            height: 30px;
            background: #5b6270;
            border-radius: 3px;
            float: left;
            position: relative;
            top: 15px;
            left: 20px;
        }

        .layout-nav {
            width: 420px;
            margin: 0 auto;
        }

        .layout-assistant {
            width: 300px;
            margin: 0 auto;
            height: inherit;
        }

        .layout-breadcrumb {
            padding: 10px 15px 0;
        }

        .layout-content {
            min-height: 200px;
            margin: 15px;
            overflow: hidden;
            background: #fff;
            border-radius: 4px;
        }

        .layout-content-main {
            padding: 10px;
        }

        .layout-copy {
            text-align: center;
            padding: 10px 0 20px;
            color: #9ea7b4;
        }
    </style>
</head>
<body id="app">
    <div class="layout">
        <i-menu mode="horizontal" theme="dark" active-name="1">
            <div class="layout-logo"></div>
            <div class="layout-nav">
                <MenuItem name="1">
                    <Icon type="ios-navigate"></Icon>
                    导航一
                </MenuItem>
                <MenuItem name="2">
                    <Icon type="ios-keypad"></Icon>
                    导航二
                </MenuItem>
                <MenuItem name="3">
                    <Icon type="ios-analytics"></Icon>
                    导航三
                </MenuItem>
                <MenuItem name="4">
                    <Icon type="ios-paper"></Icon>
                    导航四
                </MenuItem>
            </div>
        </i-menu>
        <#--<Menu mode="horizontal" active-name="1">-->
            <#--<div class="layout-assistant">-->
                <#--<MenuItem name="1">二级导航</MenuItem>-->
                <#--<MenuItem name="2">二级导航</MenuItem>-->
                <#--<MenuItem name="3">二级导航</MenuItem>-->
            <#--</div>-->
        <#--</Menu>-->
        <#--<div class="layout-breadcrumb">-->
            <#--<Breadcrumb>-->
                <#--<BreadcrumbItem href="#">首页</BreadcrumbItem>-->
                <#--<BreadcrumbItem href="#">应用中心</BreadcrumbItem>-->
                <#--<BreadcrumbItem>某应用</BreadcrumbItem>-->
            <#--</Breadcrumb>-->
        <#--</div>-->
        <div class="layout-content">
            内容区域
        </div>
        <div class="layout-copy">
            2011-2016 &copy; TalkingData
        </div>
    </div>
    <script>
        new Vue({
            el: '#app',
            data: {
                visible: false
            },
            methods: {
                show: function () {
                    this.visible = true;
                }
            }
        })
    </script>

</body>
</html>
