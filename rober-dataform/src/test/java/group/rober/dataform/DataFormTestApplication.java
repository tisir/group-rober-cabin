package group.rober.dataform;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataFormTestApplication {
    public static void main(String[] args){
        SpringApplication.run(DataFormTestApplication.class,args);
    }

}
