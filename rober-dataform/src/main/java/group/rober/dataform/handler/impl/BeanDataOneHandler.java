package group.rober.dataform.handler.impl;

import group.rober.dataform.handler.DataOneHandler;
import group.rober.dataform.model.DataForm;
import group.rober.runtime.kit.ClassKit;

import java.util.Map;

public class BeanDataOneHandler<T> extends BeanDataHandler<T> implements DataOneHandler<T> {

    public void initDataForm(DataForm dataForm, T dataObject) {

    }

    public T createDataObject(DataForm dataForm) {
        return (T) ClassKit.newInstance(dataForm.getDataModel());
    }

    public T query(DataForm dataForm, Map<String, ?> queryParameters) {
        String sql = dataForm.getQuery();
        return dataAccessor.selectOne(getFormClass(dataForm),sql,queryParameters);
    }

    public int insert(DataForm dataForm, T object) {
        return dataAccessor.insert(object);
    }

    public int update(DataForm dataForm, T object) {
        return dataAccessor.update(object);
    }

    public int save(DataForm dataForm, T object) {
        return dataAccessor.save(object);
    }

    public int delete(DataForm dataForm, T object) {
        return dataAccessor.delete(object);
    }
}
