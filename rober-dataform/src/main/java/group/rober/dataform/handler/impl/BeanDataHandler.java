package group.rober.dataform.handler.impl;

import group.rober.dataform.exception.DataFormException;
import group.rober.dataform.model.DataForm;
import group.rober.sql.core.DataAccessor;

public abstract class BeanDataHandler<T> {

    protected DataAccessor dataAccessor;

    public DataAccessor getDataAccessor() {
        return dataAccessor;
    }

    public void setDataAccessor(DataAccessor dataAccessor) {
        this.dataAccessor = dataAccessor;
    }

    protected Class<T> getFormClass(DataForm dataForm){
        Class<T> clazz = null;
        try {
            clazz = (Class<T>) Class.forName(dataForm.getDataModel());
        } catch (ClassNotFoundException e) {
            throw new DataFormException("",e);
        }
        return clazz;
    }
}
