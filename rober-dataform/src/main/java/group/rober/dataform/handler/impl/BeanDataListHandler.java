package group.rober.dataform.handler.impl;

import group.rober.dataform.handler.DataListHandler;
import group.rober.dataform.model.DataForm;
import group.rober.runtime.model.PaginationData;
import group.rober.sql.core.PaginationQuery;

import java.util.List;
import java.util.Map;

public class BeanDataListHandler<T> extends BeanDataHandler<T> implements DataListHandler<T> {

    public void initDataForm(DataForm dataForm) {

    }

    public PaginationData<T> query(DataForm dataForm, Map<String, ?> queryParameters, Map<String, ?> filterParameters, Map<String, ?> sortMap, int pageSize, int pageIndex) {

        String sql = dataForm.getQuery();
        //分页查询参数处理
        PaginationQuery query = new PaginationQuery();
        query.setQuery(sql);
        query.setIndex(pageIndex);
        query.setSize(pageSize);
        query.getParameterMap().putAll(queryParameters);

        return dataAccessor.selectListPagination(getFormClass(dataForm),query);
    }

    public int insert(DataForm dataForm, List<T> dataList) {
        return dataAccessor.insert(dataList);
    }

    public int update(DataForm dataForm, List<T> dataList) {
        return dataAccessor.update(dataList);
    }

    public int save(DataForm dataForm, List<T> dataList) {
        return dataAccessor.save(dataList);
    }

    public Integer delete(DataForm dataForm, List<T> dataList) {
        return dataAccessor.delete(dataList);
    }
}
