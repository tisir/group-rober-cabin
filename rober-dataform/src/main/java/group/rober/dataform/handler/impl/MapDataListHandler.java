package group.rober.dataform.handler.impl;

import group.rober.dataform.handler.DataListHandler;
import group.rober.dataform.model.DataForm;
import group.rober.runtime.kit.ValidateKit;
import group.rober.runtime.lang.MapData;
import group.rober.runtime.model.PaginationData;
import group.rober.sql.core.PaginationQuery;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;
import java.util.Map;

/**
 * Map列表数据处理
 */
public class MapDataListHandler extends MapDataHandler implements DataListHandler<MapData> {

    public void initDataForm(DataForm dataForm) {

    }

    public PaginationData<MapData> query(DataForm dataForm, Map<String, ?> queryParameters, Map<String, ?> filterParameters, Map<String, ?> sortMap, int pageSize, int pageIndex) {
        ValidateKit.notNull(dataForm);
        String sql = dataForm.getQuery();
        ValidateKit.notBlank(sql,"dataform={0}.{1}的query属性值为空",dataForm.getPack(),dataForm.getId());

        //分页查询参数处理
        PaginationQuery query = new PaginationQuery();
        query.setQuery(sql);
        query.setIndex(pageIndex);
        query.setSize(pageSize);
        query.getParameterMap().putAll(queryParameters);

        RowMapper<MapData> rowMapper = getDataFormRowMapper(dataForm);
        return mapDataAccessor.getMapDataQuery()
                .useRowMapperOnce(rowMapper)
                .selectListPagination(query);


    }

    public int insert(DataForm dataForm, List<MapData> dataList) {
        validateDataForm(dataForm);
        String table = dataForm.getDataModel();

        List<MapData> dbDataList = getDbMapDataList(dataForm,dataList);

        return mapDataAccessor.getMapDataUpdater()
                .useNameConverterOnce(getNameConverter(dataForm))
                .insert(table,dbDataList);
    }

    public int update(DataForm dataForm, List<MapData> dataList) {
        validateDataForm(dataForm);
        String table = dataForm.getDataModel();

        List<MapData> dbDataList = getDbMapDataList(dataForm,dataList);
        List<MapData> dbPkDataList = getDbPkMapDataList(dataForm,dataList);

        return mapDataAccessor.getMapDataUpdater()
                .useNameConverterOnce(getNameConverter(dataForm))
                .update(table,dbDataList,dbPkDataList);
    }

    public int save(DataForm dataForm, List<MapData> dataList) {
        validateDataForm(dataForm);
        String table = dataForm.getDataModel();

        List<MapData> dbDataList = getDbMapDataList(dataForm,dataList);
        List<MapData> dbPkDataList = getDbPkMapDataList(dataForm,dataList);

        return mapDataAccessor.getMapDataUpdater()
                .useNameConverterOnce(getNameConverter(dataForm))
                .save(table,dbDataList,dbPkDataList);
    }

    public Integer delete(DataForm dataForm, List<MapData> dataList) {
        validateDataForm(dataForm);
        String table = dataForm.getDataModel();

        List<MapData> dbPkDataList = getDbPkMapDataList(dataForm,dataList);
        return mapDataAccessor.getMapDataUpdater()
                .useNameConverterOnce(getNameConverter(dataForm))
                .delete(table,dbPkDataList);
    }
}
