package group.rober.dataform.service;

import group.rober.dataform.mapper.DataFormMapper;
import group.rober.dataform.model.DataForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * DataForm功能实现
 * Created by tisir<yangsong158@qq.com> on 2017-05-30
 */
@Service
public class DataFormService {
    @Autowired
    private DataFormMapper dataFormMapper;

    public DataFormMapper getDataFormMapper() {
        return dataFormMapper;
    }

    public void setDataFormMapper(DataFormMapper dataFormMapper) {
        this.dataFormMapper = dataFormMapper;
    }

    public DataForm getDataForm(String dataform){
        int lastDotIdx = dataform.lastIndexOf("-"); //URL路径中,多个点号传输会出问题
        if(lastDotIdx>0){
            String pack = dataform.substring(0,lastDotIdx).replaceAll("-",".");
            String formId = dataform.substring(lastDotIdx+1);
            return dataFormMapper.getDataForm(pack,formId);
        }else{
            return dataFormMapper.getDataForm("",dataform);
        }
    }
}
