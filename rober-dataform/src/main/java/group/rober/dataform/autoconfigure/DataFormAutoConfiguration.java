package group.rober.dataform.autoconfigure;

import group.rober.dataform.handler.impl.BeanDataListHandler;
import group.rober.dataform.handler.impl.BeanDataOneHandler;
import group.rober.dataform.handler.impl.MapDataListHandler;
import group.rober.dataform.handler.impl.MapDataOneHandler;
import group.rober.dataform.mapper.DataFormMapper;
import group.rober.dataform.mapper.impl.DataFormMapperXMLFileImpl;
import group.rober.dataform.tags.DataFormTags;
import group.rober.runtime.kit.ListKit;
import group.rober.sql.core.DataAccessor;
import group.rober.sql.core.MapDataAccessor;
import group.rober.sql.core.MapDataQuery;
import group.rober.sql.core.MapDataUpdater;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by tisir<yangsong158@qq.com> on 2017-05-13
 * 认证授权服务模块自动注入,主要完成基本配置以及自动扫描包路径的基础设置
 */
@Configuration
@EnableConfigurationProperties(DataFormProperties.class)
@ComponentScan(basePackages={"group.rober.dataform"})
public class DataFormAutoConfiguration implements InitializingBean {
    private DataFormProperties properties;

    @Autowired
    private freemarker.template.Configuration freemarkerConfig;

    public DataFormAutoConfiguration(DataFormProperties properties) {
        this.properties = properties;
    }

    @Bean("dataFormMapper")
    public DataFormMapper getDataFormMapper(){
        DataFormMapperXMLFileImpl dataFormMapper = new DataFormMapperXMLFileImpl();
        dataFormMapper.setBasePath(properties.getResourcePath());
        dataFormMapper.setVersionConflictDetection(false);
        return  dataFormMapper;
    }

    @Bean("mapDataListHandler")
    @ConditionalOnClass({MapDataAccessor.class})
    @ConditionalOnMissingBean(MapDataListHandler.class)
    public MapDataListHandler getMapDataListHandler(MapDataAccessor mapDataAccessor){
        MapDataListHandler mapDataListHandler = new MapDataListHandler();
        mapDataListHandler.setMapDataAccessor(mapDataAccessor);
        return mapDataListHandler;
    }

    @Bean("mapDataOneHandler")
    @ConditionalOnClass({MapDataAccessor.class})
    @ConditionalOnMissingBean(MapDataOneHandler.class)
    public MapDataOneHandler getMapDataOneHandler(MapDataAccessor mapDataAccessor){
        MapDataOneHandler mapDataOneHandler = new MapDataOneHandler();
        mapDataOneHandler.setMapDataAccessor(mapDataAccessor);
        return mapDataOneHandler;
    }

    @Bean("beanDataOneHandler")
    public BeanDataOneHandler<Object> getBeanDataOneHandler(DataAccessor dataAccessor){
        BeanDataOneHandler<Object> handler = new BeanDataOneHandler<Object>();
        handler.setDataAccessor(dataAccessor);
        return handler;
    }

    @Bean("beanDataListHandler")
    public BeanDataListHandler<Object> getBeanDataListHandler(DataAccessor dataAccessor){
        BeanDataListHandler<Object> handler = new BeanDataListHandler<Object>();
        handler.setDataAccessor(dataAccessor);
        return handler;
    }

    public void afterPropertiesSet() throws Exception {
        freemarkerConfig.setSharedVariable(properties.getDataformTagName(), new DataFormTags());
        //设置自动包含文件:去重合并
        freemarkerConfig.setAutoIncludes(ListKit.mergeDistinct(freemarkerConfig.getAutoIncludes(),properties.getAutoIncludes()));
    }
}
