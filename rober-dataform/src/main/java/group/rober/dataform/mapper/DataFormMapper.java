package group.rober.dataform.mapper;

import group.rober.dataform.model.DataForm;

import java.util.List;

/**
 * DatForm对象操作的一些基础服务
 * Created by tisir<yangsong158@qq.com> on 2017-05-22
 */
public interface DataFormMapper {

    public DataForm getDataForm(String pack,String formId);

    public List<DataForm> getAllDataForms();

    public int save(DataForm dataForm);
}
