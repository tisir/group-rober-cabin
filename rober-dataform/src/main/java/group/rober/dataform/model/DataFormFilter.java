package group.rober.dataform.model;

import group.rober.dataform.model.types.ElementFilterComparePattern;

import java.io.Serializable;

/**
 * DataForm的过滤器
 * Created by tisir<yangsong158@qq.com> on 2017-05-23
 */
public class DataFormFilter implements Serializable,Cloneable{

    protected String code;
    protected String name;
    protected String bindFor;
    protected Boolean available = true;
    protected Boolean quick = false;
    protected ElementFilterComparePattern comparePattern = ElementFilterComparePattern.StartWith;

    public String getCode() {
        return code;
    }

    public DataFormFilter setCode(String code) {
        this.code = code;
        return this;
    }

    public String getName() {
        return name;
    }

    public DataFormFilter setName(String name) {
        this.name = name;
        return this;
    }

    public String getBindFor() {
        return bindFor;
    }

    public DataFormFilter setBindFor(String bindFor) {
        this.bindFor = bindFor;
        return this;
    }

    public Boolean getAvailable() {
        return available;
    }

    public DataFormFilter setAvailable(Boolean available) {
        this.available = available;
        return this;
    }

    public Boolean getQuick() {
        return quick;
    }

    public DataFormFilter setQuick(Boolean quick) {
        this.quick = quick;
        return this;
    }

    public ElementFilterComparePattern getComparePattern() {
        return comparePattern;
    }

    public DataFormFilter setComparePattern(ElementFilterComparePattern comparePattern) {
        this.comparePattern = comparePattern;
        return this;
    }
}
