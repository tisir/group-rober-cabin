(function($){
    var api = {};
    function toggleGroup(groupDom){
        var legendTitle = $(".rb-legend-title",groupDom);
        var titleIcon = $(".fa",legendTitle);
        if(titleIcon.hasClass('fa-minus')){//表示展开状态，需要收起
            collapseGroup(groupDom);
        }else{
            expandGroup(groupDom);
        }
    };
    function expandGroup(groupDom){
        var legendTitle = $(".rb-legend-title",groupDom);
        var titleIcon = $(".fa",legendTitle);
        if(titleIcon.hasClass('fa-plus')){//表示展开状态，需要收起
            $(".rb-fieldset-body",groupDom).show();
            $(".rb-fieldset-collapse-hint",groupDom).hide();
            titleIcon.addClass("fa-minus").removeClass("fa-plus");
        }
    };
    function collapseGroup(groupDom){
        var legendTitle = $(".rb-legend-title",groupDom);
        var titleIcon = $(".fa",legendTitle);
        if(titleIcon.hasClass('fa-minus')){//表示展开状态，需要收起
            $(".rb-fieldset-body",groupDom).hide();
            $(".rb-fieldset-collapse-hint",groupDom).show();
            titleIcon.addClass("fa-plus").removeClass("fa-minus");
        }
    };

    function render(el){
        $(".rb-fieldset").delegate(".rb-legend-title,.rb-fieldset-collapse-hint","click", function(){
            toggleGroup($(this).parents(".rb-fieldset"));
        });
    };

    console.log('detail-js');

    //把公开的API放这里来
    api.render = render;
    return api;

    //所有标题节点作事件绑定
})($);