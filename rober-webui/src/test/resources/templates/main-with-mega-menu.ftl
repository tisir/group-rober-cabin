<@body1 title="平铺化菜单" class="page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
<link rel="stylesheet" href="${ctxPath}/assets/iconfont/iconfont.css?${currentTimeMillis}">
<link rel="stylesheet" href="${ctxPath}/assets/css/main-content2.css?${currentTimeMillis}">
<link rel="stylesheet" href="${ctxPath}/webui/mega-menu/css/mega-menu.css?${currentTimeMillis}">

<#--<link rel="stylesheet" href="${ctxPath}/assets/css/theme/theme-orange1.css?${currentTimeMillis}">-->
<#--<link rel="stylesheet" href="${ctxPath}/webui/mega-menu/css/mega-menu-orange1.css?${currentTimeMillis}">-->

<nav class="nav navbar-default navbar-static-top rb-nav-wrapper" role="navigation">
    <!-- BEGIN MEGA MENU -->
    <div class="rb-nav-container">
        <div class="navbar-header rb-navbar-logo">
            <a href="javascript:void(0);" class="rb-navbar-logo-link">
                <span><i class="rb-icon rb-icon-amarsoft"></i></span>
            </a>
        </div>

        <div class="navbar-nav navbar-left rb-nav-menu-wrapper">
            <ul class="nav rb-nav-menu-container" id="rb-nav-menu-container">
                <script type="text/template" id="pulldown-item-tpl">
                    <li><div class="{{pulldownClass}}"><i class="{{iconClass}}"></i>{{name}}</div></li>
                </script>
                <script type="text/template" id="menu-item-tpl">

                </script>
            </ul>
            <div>
                <span class='rb-nav-slip' id='rb-nav-slip-left'><i></i></span>
                <span class='rb-nav-slip' id='rb-nav-slip-right'><i></i></span>
            </div>
        </div>

        <div class="container rb-nav-pulldown" id="rb-nav-pulldown-container">
        </div>

        <ul class="nav navbar-nav navbar-right rb-nav-right">
            <!-- BEGIN USER LOGIN DROPDOWN -->
            <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                    <img width="40px" height="40px" alt="" class="img-circle"/>
                    <span>测试用户</span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-default">
                    <li>
                        <a href="javascript:void(0);"><i class="icon-user"></i>我的信息</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="javascript:void(0);"><i class="icon-user"></i>修改密码</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);"><i class="icon-lock"></i>锁屏</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);"><i class="icon-key"></i>注销</a>
                    </li>
                </ul>
            </li>
            <!-- END USER LOGIN DROPDOWN -->
        </ul>
    </div>
    <!-- END MEGA MENU -->


</nav>


<!-- BEGIN CONTENT -->
<div class="page-content-wrapper" id="main-content">
    <!--<div id="spinner">-->
    <!--<i class="fa fa-spinner spinner">-->
    <!--</i>-->
    <!--</div>-->
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" id="main-content-container">
        <div class="main-tabs-container">
            <ul class="nav nav-tabs" id="nav-tabs-visable">

            </ul>

            <li class="dropdown pull-right tabsmore">
                <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);" aria-expanded="false">
                    <span class="badge" ng-cloak data-ng-bind="tabsCollapse.length"></span>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu" id="nav-tabs-collapse">

                </ul>
            </li>

            <li class="dropdown pull-right tabdrop">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true">
                    <span class="glyphicon glyphicon-remove" style="font-size: large;color:#aaa"></span>
                    <b class="caret"></b>
                </a>

                <ul class="dropdown-menu dropdown-menu-default">
                    <li><a onclick="mainController.closeAllTabs()" data-toggle="tab">关闭所有</a></li>
                    <li><a onclick="mainController.closeOtherTabs()" data-toggle="tab">关闭其他</a></li>
                </ul>
            </li>

            <div class="tab-content" id="tab-content-container">
                <div class="tab-pane" id="tab-pane">
                    <ol class="breadcrumb">
                        <div class="img rb-icon rb-icon-0010home"></div>
                        <li data-ng-repeat="bcruItem in tabItem.breadcrumbData">
                            <i ng-if="$index==1"></i>
                            <span data-ng-bind="bcruItem.name"></span>
                        </li>
                    </ol>
                </div>
                <!--<div id="containter-{{contentItem.id}}" class="tab-pane-content" ng-repeat="contentItem in tabContents" ng-class='contentItem .active?"active":""'></div>-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->


<!-- BEGIN FOOTER -->
<div class="footer">
    <div class="footer-inner"> 2017 &copy; 安硕信息
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->



<script src="${ctxPath}/lang/array.js" charset="utf-8"></script>
<script src="${ctxPath}/lang/string.js" charset="utf-8"></script>
<script src="${ctxPath}/assets/js/rax.js" charset="utf-8"></script>
<script src="${ctxPath}/assets/js/rax-control.js" charset="utf-8"></script>
<script src="${ctxPath}/assets/mock/MenuData.js?${currentTimeMillis}" charset="utf-8"></script>
<script src="${ctxPath}/assets/mock/controller.js?${currentTimeMillis}" charset="utf-8"></script>
<script src="${ctxPath}/assets/mock/content.js?${currentTimeMillis}" charset="utf-8"></script>
<script>
    $(function () {
        mainController.rendMenuItem(_mockMenuData.body)
    })
</script>
</@body1>