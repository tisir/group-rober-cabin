<@body title="平铺化菜单" class="page-sidebar-closed-hide-logo page-content-white page-sidebar-fixed">
<link rel="stylesheet" href="${ctxPath}/assets/iconfont/iconfont.css?${currentTimeMillis}">
<link rel="stylesheet" href="${ctxPath}/assets/css/main-content2.css?${currentTimeMillis}">
<link rel="stylesheet" href="${ctxPath}/webui/mega-menu/css/mega-menu.css?${currentTimeMillis}">

<#--<link rel="stylesheet" href="${ctxPath}/assets/css/theme/theme-orange1.css?${currentTimeMillis}">-->
<#--<link rel="stylesheet" href="${ctxPath}/webui/mega-menu/css/mega-menu-orange1.css?${currentTimeMillis}">-->

<nav class="nav navbar-default navbar-static-top rb-nav-wrapper" role="navigation">
    <!-- BEGIN MEGA MENU -->
    <div class="rb-nav-container">
        <div class="navbar-header rb-navbar-logo">
            <a href="javascript:void(0);" class="rb-navbar-logo-link">
                <span><i class="rb-icon rb-icon-amarsoft"></i></span>
            </a>
        </div>

        <div class="navbar-nav navbar-left rb-nav-menu-wrapper">
            <ul class="nav rb-nav-menu-container" id="rb-nav-menu-container">

                <script id="rb-nav-menu-item-tmpl" type="text/x-dot-template">
                </script>

                <script id="rb-nav-menu-item-tmpl" type="text/x-dot-template">
                </script>

            </ul>
            <div>
                <span class='rb-nav-slip' id='rb-nav-slip-left'><i></i></span>
                <span class='rb-nav-slip' id='rb-nav-slip-right'><i></i></span>
            </div>
        </div>

        <div class="container rb-nav-pulldown" id="rb-nav-pulldown-container">
        </div>

        <ul class="nav navbar-nav navbar-right rb-nav-right">
            <!-- BEGIN USER LOGIN DROPDOWN -->
            <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                    <img width="40px" height="40px" alt="" class="img-circle"/>
                    <span>测试用户</span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-default">
                    <li>
                        <a href="javascript:void(0);"><i class="icon-user"></i>我的信息</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="javascript:void(0);"><i class="icon-user"></i>修改密码</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);"><i class="icon-lock"></i>锁屏</a>
                    </li>
                    <li>
                        <a href="javascript:void(0);"><i class="icon-key"></i>注销</a>
                    </li>
                </ul>
            </li>
            <!-- END USER LOGIN DROPDOWN -->
        </ul>
    </div>
    <!-- END MEGA MENU -->

</nav>





<script src="${ctxPath}/lang/array.js" charset="utf-8"></script>
<script src="${ctxPath}/lang/string.js" charset="utf-8"></script>
<script src="${ctxPath}/assets/js/rax.js" charset="utf-8"></script>
<script src="${ctxPath}/assets/js/rax-control.js" charset="utf-8"></script>
<script src="${ctxPath}/assets/mock/MenuData.js?${currentTimeMillis}" charset="utf-8"></script>
<script src="${ctxPath}/assets/mock/controller.js?${currentTimeMillis}" charset="utf-8"></script>
<script src="${ctxPath}/assets/mock/content.js?${currentTimeMillis}" charset="utf-8"></script>
<script>
    $(function () {
        function getCurrentMilliseconds(){
            var d = new Date();
            return d.getHours()*3600*1000+d.getMinutes()*60*1000+d.getSeconds()*1000+d.getMilliseconds();
        }
//        mainController.rendMenuItem(_mockMenuData.body)
        $(".navbar-default").megamenu({
            menudata:menudata//假设的,
            on:{
                logoClick:function(){window.location = "http://rober.group"}
                menuItemClick:function(node){console.log("clicked",node);}
            }
        });
    })
</script>
</@body>