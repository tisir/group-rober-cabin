package group.rober.webui;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebuiTestApplication {
    public static void main(String[] args){
        SpringApplication.run(WebuiTestApplication.class,args);
    }
}
