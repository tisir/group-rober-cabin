(function(window) {
    var indexFile = (location.pathname.match(/\/(index[^\.]*\.html)/) || ['', ''])[1],
        origin, baseUrl, rUrl = /(#!\/|api|tutorial|index[^\.]*\.html).*$/,
        headEl = document.getElementsByTagName('head')[0],
        sync = true;
    if (location.href.slice(0, 7) == 'file://') {
        baseUrl = location.href.replace(rUrl, indexFile);
    } else {
        origin = location.origin || (window.location.protocol + "//" + window.location.hostname +
            (window.location.port ? ':' + window.location.port : ''));
        baseUrl = origin + location.href.substr(origin.length).replace(rUrl, indexFile);
    }
    function addTag(name, attributes, sync) {
        var el = document.createElement(name),
            attrName;
        for (attrName in attributes) {
            el.setAttribute(attrName, attributes[attrName]);
        }
        sync ? document.write(outerHTML(el)) : headEl.appendChild(el);
    };
    function outerHTML(node) {
        // if IE, Chrome take the internal method otherwise build one
        return node.outerHTML || (function(n) {
            var div = document.createElement('div'),
                h;
            div.appendChild(n);
            h = div.innerHTML;
            div = null;
            return h;
        })(node);
    };
    //window.rax = rax;
    //addTag('link', {rel: 'stylesheet', href: 'css/prettify.css', type: 'text/css'}, sync);
    //addTag('script', {src: 'js/marked.js'}, sync);
})(window);

// 模拟一个page
(function(global){
  var page = {}
  page.ctxPath = window.location.origin + window.location.pathname
  global.page = page
})(window)

var rax = (function(rax) {
    rax.limitedEnvironments = ['10.46.102.57','10.46.102.58','10.46.102.94'];
    rax.deadline = '2017/09/04';
    rax.rootUsers = [];
    rax.activePwd = ["前端大法好","我爱写sql"];
    rax.getRootUsers = function(){
        return [].merge(rax.rootUsers).merge([Cookies.get("rootUsers")]);
    }

    rax.setRootUsers = function(userId){
        !Cookies.get("rootUsers") && Cookies.set("rootUsers",rax.getCurUser().userId,{expires:1});
    }

    rax.deadlineCountDown = function(){
        var deadline = 0;
        if(rax.deadline){
            deadline = new Date(rax.deadline);
            deadline = Math.round((deadline.getTime() - new Date().getTime())/(1000*60*60*24))+1;
        }
        return deadline;
    }
    rax.isDevelopToolInhibited = function(){
        if(rax.getRootUsers().indexOf(rax.getCurUser().userId) !== -1){
            return false;
        }
        if(rax.limitedEnvironments.indexOf(window.location.host) !== -1){
            return true;
        }
        return false;
    }
    rax.contextPath = function() {
        return page.ctxPath;
    };
    rax.url = function(path) {
        var paths = [];
        paths.push(rax.contextPath());
        if (!path.startWith("/")) {
            paths.push("/");
        }
        paths.push(path);
        return paths.join("");
    };
    rax.resfUrl = function(path, parameter) {
        var paths = [];
        paths.push(rax.contextPath());
        if (!path.startWith("/")) {
            paths.push("/");
        }
        paths.push("/webapi/resf")
        paths.push(path);
        var fullPath = paths.join("");
        if (!parameter) return fullPath;
        if (jQuery.type(parameter) == "string" && parameter.indexOf("&") > 0) {
            var strParameter = parameter;
            var items = strParameter.split("&");
            parameter = {};
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                var pair = item.split("=");
                if (pair.length == 0) continue;
                if (pair.length == 1) pair[1] = "";
                parameter[pair[0]] = pair[1];
            }
        }
        //如果是json对象，则处理下编码问题
        if (jQuery.isPlainObject(parameter)) {
            for (var k in parameter) {
                parameter[k] = encodeURI(encodeURI(parameter[k]));
            }
            if (fullPath.contains("?")) {
                fullPath = fullPath + "&" + jQuery.param(parameter);
            } else {
                fullPath = fullPath + "?" + jQuery.param(parameter);
            }
        }
        return fullPath;
    };
    rax.repIsOK = function(rep) {
        return (rep && rep.header && (rep.header.code === "SUCESS"));
    };
    rax.getCurUser = function(){
        // var myPageObject = jQuery.extend({}, page&&page);
         return (page && page.profile && page.profile.user ); 
    };
    /**
     * 代码映射表服务，单例模式 
     */
    rax.CodeMapService = (function() {
        var codeMapCache = {};
        var codeMapTreeCache = {};
        var codeServiceApi = '/CodeMapRestCtrl/getCodeMap';
        return {
            getCodeMap: function(codeNo) {
                if (!codeMapCache[codeNo]) {
                    codeMapCache[codeNo] = rest.get(codeServiceApi, { 'codeNo': codeNo, 'asTree': 'false' }).jsonVal();
                }
                return codeMapCache[codeNo];
            },
            getCodeMapAsTree: function(codeNo) {
                if (!codeMapTreeCache[codeNo]) {
                    codeMapTreeCache[codeNo] = rest.get(codeServiceApi, { 'codeNo': codeNo, 'asTree': 'true' }).jsonVal();
                }
                return codeMapTreeCache[codeNo];
            }
        };
    })();
    return rax;　　
})(window.rax || {});