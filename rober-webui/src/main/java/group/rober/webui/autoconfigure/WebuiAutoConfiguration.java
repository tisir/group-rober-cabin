package group.rober.webui.autoconfigure;


import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

@EnableConfigurationProperties(WebuiProperties.class)
@ComponentScan(basePackages = "group.rober.webui")
public class WebuiAutoConfiguration {
    protected WebuiProperties properties;

    public WebuiAutoConfiguration(WebuiProperties properties) {
        this.properties = properties;
    }
}
