package group.rober.webui.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "group.rober.webui", ignoreUnknownFields = true)
public class WebuiProperties {
}
