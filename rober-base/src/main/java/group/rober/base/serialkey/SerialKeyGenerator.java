package group.rober.base.serialkey;


public interface SerialKeyGenerator<T> {
    public T next();
    public T next(Object ... objects);
}
