package group.rober.base.autoconfigure;

import com.google.common.collect.Lists;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties(prefix = "group.rober.base", ignoreUnknownFields = true)
public class BaseProperties {


    private String createdTimePropertyName = "createdTime";

    private String updatedTimePropertyName = "updatedTime";

    private String viewPath = "/views";

    private String numberFormat = "#";

    private List<String> autoIncludes = Lists.newArrayList("/base/macro/body.ftl");

    private boolean productionModel = false;    /*是否生产模式*/

    private long multipartMaxFileSize = 1024L * 1024L * 100;//最大上传文件大小，100M


    public String getCreatedTimePropertyName() {
        return createdTimePropertyName;
    }

    public void setCreatedTimePropertyName(String createdTimePropertyName) {
        this.createdTimePropertyName = createdTimePropertyName;
    }

    public String getUpdatedTimePropertyName() {
        return updatedTimePropertyName;
    }

    public void setUpdatedTimePropertyName(String updatedTimePropertyName) {
        this.updatedTimePropertyName = updatedTimePropertyName;
    }

    public long getMultipartMaxFileSize() {
        return multipartMaxFileSize;
    }

    public void setMultipartMaxFileSize(long multipartMaxFileSize) {
        this.multipartMaxFileSize = multipartMaxFileSize;
    }

    public String getViewPath() {
        return viewPath;
    }

    public void setViewPath(String viewPath) {
        this.viewPath = viewPath;
    }

    public List<String> getAutoIncludes() {
        return autoIncludes;
    }

    public void setAutoIncludes(List<String> autoIncludes) {
        this.autoIncludes = autoIncludes;
    }

    public boolean isProductionModel() {
        return productionModel;
    }

    public void setProductionModel(boolean productionModel) {
        this.productionModel = productionModel;
    }

    public String getNumberFormat() {
        return numberFormat;
    }

    public void setNumberFormat(String numberFormat) {
        this.numberFormat = numberFormat;
    }
}
