package group.rober.base.controller;

import group.rober.runtime.kit.StringKit;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Api(value = "RoberApi-Base", description = "基础应用")
@Controller
@RequestMapping("/base")
public class BaseController {
    protected Logger logger = LoggerFactory.getLogger(BaseController.class);
    private static String redirectPrefix = "/base/redirect/";

    @ApiOperation(value = "页面服务端跳转")
    @RequestMapping(path = "/redirect/**",method = {RequestMethod.GET,RequestMethod.POST})
    public ModelAndView redirect(HttpServletRequest request){
        String servletPath = request.getServletPath();
        if(StringKit.isNotBlank(servletPath)&&servletPath.startsWith(redirectPrefix)){
            String viewName = servletPath.substring(redirectPrefix.length());
            logger.debug("redirect:"+servletPath+"->"+viewName);
            Map<String,Object> vars = new HashMap<String,Object>();
            return new ModelAndView(viewName,vars);
        }
        return null;
    }
    @ApiOperation(value = "当前页面全局JS对象")
    @RequestMapping(path = "/current-page.js",method = {RequestMethod.GET,RequestMethod.POST})
    public void renderCurrentPageVariables(HttpServletRequest request, HttpServletResponse response){
        StringBuilder script = new StringBuilder("");
        script.append("var page={");
        script.append("\"");
        script.append("}");
    }
}
